<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/*
 * Save selection
 * */
class LJC_SaveCartDB{
	private $instance_cart = null;
	private $save_array = array();
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function set_option(){
		if($this->get_cart_db_instance()->has_cart_items()){
			$this->set_array();
			$ljc_session_save_selection = $this->get_array();
			if( !$this->get_db() ){
				update_option($this->get_prefix_option(), $ljc_session_save_selection);
			}
		}
	}
	
	public function get_array(){
		return $this->save_array;
	}

	public function set_array(){
		/*
		 * array(setting_id, diamond_id)
		 * */
		$this->save_array['save_selection_pair'] = array();
	}
	
	public function add_pair($setting_id, $diamond_id){
		$get = $this->get_db();
		$cart = LJC_CartDB::get_instance()->get();
		$key = $setting_id . $diamond_id;
		if( $get ){
			//if( !array_key_exists($key, $get['save_selection_pair']) ){
				$product_array_id = array();
				$variation_id = 0;
				$ring_size = 0;
				foreach($cart['cart']['settings'] as $k => $v){
					if( $v['product_id'] == $setting_id ){
						$variation_id = $v['variation_id'];
					}
					if( isset($v['ring_size']) ){
						$ring_size = $v['ring_size'];
					}
				}
				$get['save_selection_pair'][$key] = array(
					'setting' => array('product_id'=>$setting_id,'variation_id'=>$variation_id,'ring_size'=>$ring_size),
					'diamond' => $diamond_id
				);
				return update_option($this->get_prefix_option(), $get);
			//}else{
				//selection setting and diamond id exists
				//return 2;
			//}
		}
		return false;
	}

	public function get_db(){
		return get_option($this->get_prefix_option());
	}
	
	public function remove_db(){
		return delete_option($this->get_prefix_option());
	}
	
	public function get_prefix_option(){
		$user_id = $this->get_cart_instance()->current_session_id();
		return 'ljc_cart_' . $user_id . '_save';
	}
	
	public function get_cart_instance(){
		return LJC_Cart::get_instance();
	}

	public function get_cart_db_instance(){
		return LJC_CartDB::get_instance();
	}
	
	public function ajax_save_selection(){
		$data = $_POST;
		$msg = 'fail';
		$status = 0;
		$setting_id = 0;
		$diamond_id = 0;
		if( isset($data['setting_id']) && $data['setting_id'] != 0 ){
			$setting_id = $data['setting_id'];
		}
		if( isset($data['diamond_id']) && $data['diamond_id'] != 0 ){
			$diamond_id = $data['diamond_id'];
		}
		if( $setting_id != 0 && $diamond_id != 0 ){
			$ret = $this->add_pair($setting_id, $diamond_id);
			/*if( $ret == 2 ){
				$msg = ljc_verbage('selections_duplicate', false);
				$status = 2;
			}else{
				$msg = ljc_verbage('selections_success',false);
				$status = 1;
			}*/
			if( $ret ){
				$msg = ljc_verbage('selections_success',false);
				$status = 1;
			}
		}
		echo json_encode(
			array(
				'msg'=>$msg,
				'data'=>$data,
				'status'=>$status
			)
		);
		die();
	}
	
	public function ajax_get_save_selection(){
		$saved_selections = ljc_query_saved_selection();
		$saved_selections_template_part = ljc_public_partials() .'cart/ljc-save-selection-part.php';
		require_once( $saved_selections_template_part );
		die();
	}
	
	public function get_saved_selection_by_key($key){
		$get_item = array();
		$get = $this->get_db();
		if( array_key_exists($key,$get['save_selection_pair']) ){
			$get_item = array(
				'key' => $key,
				'items'=>$get['save_selection_pair'][$key]
			);
		}
		return (object)$get_item;
	}
	
	public function remove_item($key){
		$item = $this->get_saved_selection_by_key($key);
		//retrieve the saved db first
		$get = $this->get_db();
		//remove from array
		unset($get['save_selection_pair'][$item->key]);
		//update it
		return update_option($this->get_prefix_option(), $get);
	}
	
	public function ajax_remove_save_selection(){
		$get = $this->get_db();
		$setting_id = 0;
		$diamond_id = 0;
		$msg = '';
		$status = 0;
		$data = array();
		if( isset($_POST['setting_id']) ){
			$setting_id = $_POST['setting_id'];
		}
		if( isset($_POST['diamond_id']) ){
			$diamond_id = $_POST['diamond_id'];
		}
		$key = $setting_id . $diamond_id;
		$ret = $this->get_saved_selection_by_key($key);
		if( $this->remove_item($ret->key) ){
			$msg = ljc_verbage('ajax_temp_cart_successfully_delete_item', false);
			$status = 1;
		}
		echo json_encode(
			array(
				'msg'=>$msg,
				'data'=>$data,
				'status'=>$status
			)
		);
		die();
	}
			
	public function __construct(){
		$this->set_option();
	}
	
}//class

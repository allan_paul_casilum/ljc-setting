<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_Session{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function get_id(){
		if ( is_user_logged_in() ) {
            return get_current_user_id();
		}else{
			return session_id();
		}
	}
	
	public function init(){
		if(!session_id()) {
			session_start();
		}
	}
	
	public function destroy(){
		session_destroy();
	}
	
	public function __construct(){
		$this->init();
	}
	
}

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_BreadcrumbShortcode{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function init_shortcode(){
		ob_start();
		global $show_breadcrumb;
		$show_breadcrumb = true;
		require_once( ljc_public_partials() .'breadcrumb-steps.php' );
		$output = ob_get_clean();
		return $output;
	}
	
	public function __construct(){
		add_shortcode('ljc_breadcrumb_steps', array($this,'init_shortcode'));
	}
	
}//class

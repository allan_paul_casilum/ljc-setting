<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_DiamondComparisonDB{
	protected static $instance = null;
	private $option_name = 'diamond_comparison_';
	private $option_name_created_at = 'diamond_comparison_created_at_';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function show(){
	
	}
	
	public function get_option_name_comparison(){
		$session_id = isset($_COKKIE['PHPSESSID']) ? $_COKKIE['PHPSESSID'] : LJC_Session::get_instance()->get_id();
		return $this->option_name . $session_id;
	}

	public function get_option_name_comparison_created_at(){
		$session_id = isset($_COKKIE['PHPSESSID']) ? $_COKKIE['PHPSESSID'] : LJC_Session::get_instance()->get_id();
		return $this->option_name_created_at . $session_id;
	}
	
	public function get_data(){
		return $this->has_db() ? get_option($this->get_option_name_comparison()) : false;
	}
	
	public function has_db(){
		return get_option($this->get_option_name_comparison()) ? true:false;
	}
	
	public function count_comparison(){
		if( $this->get_data() ){
			return count($this->get_data());
		}
		return 0;
	}
	
	public function store_db($product_id){
		$array_id = array();
		if( $this->has_db() ){
			$db = get_option($this->get_option_name_comparison());
			if( !in_array($product_id, $db ) ){
				array_push($db, $product_id);
				return update_option($this->get_option_name_comparison(), $db);
			}
		}else{
			$array_id = array($product_id);
			update_option($this->get_option_name_comparison_created_at(), date('Y-m-d H:i:s'));
			return update_option($this->get_option_name_comparison(), $array_id);
		}
		
		return false;
	}

	public function remove_db($product_id){
		if( $this->has_db() ){
			$db = get_option($this->get_option_name_comparison());
			$key = array_search($product_id, $db );
			if( $key || count($db) >= 1 ){
				if( count($db) == 1 ){
					$db = array();
				}else{
					unset($db[$key]);
					$db = array_values($db);
				}
				return update_option($this->get_option_name_comparison(), $db);
			}
		}
		
		return false;
	}
	
	public function ajax_ljc_comparison_store_db(){
		$res = array(
			'msg'=>'fail',
			'status'=>0,
		);
		if( isset($_POST['id']) ){
			if( $this->store_db($_POST['id']) ){
				$res['msg'] = 'success';
				$res['status'] = 1;
			}
		}
		echo json_encode($res);
		die();	
	}
	
	public function ajax_ljc_del_comparison_db(){
		$res = array(
			'msg'=>'fail',
			'status'=>0,
		);
		if( isset($_POST['id']) ){
			if( $this->remove_db($_POST['id']) ){
				$res['msg'] = 'success';
				$res['status'] = 1;
			}
		}
		echo json_encode($res);
		die();
	}
	
	public function count(){
		if( $this->has_db() ){
			$db = get_option($this->get_option_name_comparison());
			return count($db);
		}
		
		return 0;
	}
		
	public function __construct(){}
	
}//class

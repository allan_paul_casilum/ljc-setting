<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if( is_wp_session_manager_install() ){
	class LJC_CartSession{
		private $wp_session_manager = null;
		private $ljc_session_cart = array();
		protected static $instance = null;
		/**
		 * Return an instance of this class.
		 *
		 * @since     1.0.0
		 *
		 * @return    object    A single instance of this class.
		 */
		public static function get_instance() {

			/*
			 * @TODO :
			 *
			 * - Uncomment following lines if the admin class should only be available for super admins
			 */
			/* if( ! is_super_admin() ) {
				return;
			} */

			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}
		
		public function set_wp_session_manager(){
			global $wp_session;
			$this->wp_session_manager = $wp_session;
		}
		
		public function get_wp_session_manager(){
			return $this->wp_session_manager;
		}
		
		public function set_cart(){
			$cart = $this->get();
			if( !$cart['has_items_in_cart'] ){
				$this->ljc_session_cart = array(
					'is_final' => false,
					'has_items_in_cart' => false,
					'cart' => array()
				);
			}
		}
		
		public function add_cart($key, $product_id){
			$get = $this->get();
			$get['cart'][$key][] = $product_id;
		}	
		
		public function get(){
			return $this->wp_session_manager;
		}

		public function get_cart(){
			return $this->wp_session_manager['cart'];
		}

		public function reset(){
			global $wp_session_unset;
			wp_session_unset();
		}
		
		public function __construct(){
			$this->set_wp_session_manager();
			$this->set_cart();
		}
		
	}
}//if

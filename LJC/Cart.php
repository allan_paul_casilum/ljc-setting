<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_Cart{
	private $ljc_session_cart;
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	
	public function get_wc_session_cookie(){
		return ljc_get_session_id();
	}
	
	public function get_wc_session_id(){
		$session_id = $this->get_wc_session_cookie();
		return $session_id;
	}
	
	public function current_session_id(){
		$user = wp_get_current_user();
		$id = null;
		if( is_user_logged_in() ){
			$id = $user->ID;
		}else{
			$id = $this->get_wc_session_id();
		}
		return $id;
	}
	
	public function set_cart_array(){
		$this->ljc_session_cart = array(
			'has_added_to_cart' => false,
			'cart' => array()
		);
	}
	
	public function get_cart_array(){
		return $this->ljc_session_cart;
	}

	public function get_cart_array_key($key){
		if( isset($this->ljc_session_cart[$key]) ){
			return $this->ljc_session_cart[$key];
		}
		return false;
	}
	
	public function has_added_to_cart($has_added_to_cart = true){
		$get_has_added_to_cart = $this->get_cart_array_key('has_added_to_cart');
		$get_has_added_to_cart = $has_added_to_cart;
	}
	
	public function get_has_added_to_cart(){
		return $this->get_cart_array_key('has_added_to_cart');
	}
	
	/**
	 * this will delete saved cart and saved selection
	 * */
	public function delete_db(){
		LJC_CartDB::get_instance()->remove_db();
		LJC_SaveCartDB::get_instance()->remove_db();
	}
		
	public function __construct(){
		$this->set_cart_array();
	}
	
}//class

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_WooVariations{
	protected static $instance = null;
	protected $attribute_prefix;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function set_variations_prefix(){
		$config = mwp_config_ljc();
		$this->attribute_prefix = $config['woo_variations']['pa_prefix'];
	}
	
	public function get_variations_attribute_name(){
		$config = mwp_config_ljc();
		return $config['woo_attribute_prefix']['pa_attribute'];
	}
	
	public function variation_settings_fields($loop, $variation_data, $variation){
		global $post;
		$variation_data_prefix = $this->get_variations_attribute_name();

		$top_img = get_post_meta($post->ID, $variation_data[$variation_data_prefix] . '_top_img', true);
		$side_img = get_post_meta($post->ID, $variation_data[$variation_data_prefix] . '_side_img', true);
		$three_sixty_video = get_post_meta($post->ID, $variation_data[$variation_data_prefix] . '_three_sixty_video', true);
		?>
		<div class="techriver-ljc-varations-custom-fields-<?php echo $loop;?>">
				<div class="top-img">
					<?php if ( $top_img ) : ?>
						<img class="img-review-<?php echo $loop;?> top-img-uploaded-<?php echo $loop;?>" src="<?php echo $top_img['img_url']; ?>" alt="" style="max-width:30%;" />
					<?php endif; ?>
					<!-- Your add & remove image links -->
					<p class="hide-if-no-js">
						<a class="ljc-upload-img " 
						   href="<?php echo $top_img['img_url'] ?>" data-loop-id="<?php echo $loop;?>" data-loop-img="top">
							<?php 
								if ( $top_img  ) { 
									_e('Change top image', 'woocommerce');
								}else{
									_e('Set top image', 'woocommerce');
								}
							?>
						</a>
						<?php if ( $top_img  ) { ?>
						<br>
						<a class="ljc-delete-img " 
						  href="#" data-loop-id="<?php echo $loop;?>" data-loop-img="top">
							<?php _e('Remove this top image' , 'woocommerce') ?>
						</a>
						<?php } ?>
					</p>

					<!-- A hidden input to set and post the chosen image id -->
					<input class="top-img-id-<?php echo $loop;?>" name="top-img-id-<?php echo $loop;?>" type="hidden" value="<?php echo $loop; ?>" />
					<input class="top-url-img-<?php echo $loop;?>" name="top-url-img-<?php echo $loop;?>" type="text" value="<?php echo esc_attr( $top_img['img_url'] ); ?>" style="width:100%;" />
					<input class="top-img-postid-<?php echo $loop;?>" name="top-img-postid-<?php echo $loop;?>" type="hidden" value="<?php echo $top_img['post_id']; ?>"/>
				</div>
				<div class="side-img">
					<?php if ( $side_img ) : ?>
						<img class="img-review-<?php echo $loop;?> side-img-uploaded-<?php echo $loop;?>" src="<?php echo $side_img['img_url'] ?>" alt="" style="max-width:30%;" />
					<?php endif; ?>
					<!-- Your add & remove image links -->
					<p class="hide-if-no-js">
						<a class="ljc-upload-img " 
						   href="<?php echo $side_img['img_url'] ?>" data-loop-id="<?php echo $loop;?>" data-loop-img="side">
							<?php 
								if ( $top_img  ) { 
									_e('Change side image', 'woocommerce');
								}else{
									_e('Set side image', 'woocommerce');
								}
							?>
						</a>
						<?php if ( $side_img  ) { ?>
						<br>
						<a class="ljc-delete-img " 
						  href="#" data-loop-id="<?php echo $loop;?>" data-loop-img="side">
							<?php _e('Remove this side image', 'woocommerce') ?>
						</a>
						<?php } ?>
					</p>

					<!-- A hidden input to set and post the chosen image id -->
					<input class="side-img-id-<?php echo $loop;?>" name="side-img-id-<?php echo $loop;?>" type="hidden" value="<?php echo esc_attr( $loop ); ?>" />
					<input class="side-url-img-<?php echo $loop;?>" name="side-url-img-<?php echo $loop;?>" type="text" value="<?php echo esc_attr( $side_img['img_url'] ); ?>" style="width:100%;"/>
					<input class="side-img-postid-<?php echo $loop;?>" name="side-img-postid-<?php echo $loop;?>" type="hidden" value="<?php echo $side_img['post_id']; ?>" style="width:100%;"/>
				</div>
				<div class="video-three-sixty">
					<p>
						360 degree view Video<br>
						<input type="text" name="three-sixty-video-<?php echo $loop;?>" class="three-sixty-video-<?php echo $loop;?>" style="width:100%;" value="<?php echo $three_sixty_video['url'];?>">
					</p>
				</div>
		</div>
		<?php
	}
	
	public function save_variation_settings_fields($variation_id, $i){
		
		if( $_POST && $_POST['action'] == 'woocommerce_save_variations' ) {
			$variation_data_prefix = $this->get_variations_attribute_name();
			$top_url_img_i = 'top-url-img-'.$i;
			$side_url_img_i = 'side-url-img-'.$i;
			if( $_POST['top-img-postid-' . $i] == 0 && trim($top_url_img_i) == '' ){
				delete_post_meta( $_POST['product_id'], $_POST[$variation_data_prefix][$i] . '_top_img' );
			}else{
				$top_img_array = array(
					'idx' => $i,
					'variation_id', $variation_id,
					'img_url' => $_POST[$top_url_img_i],
					'variation_attribute' => $_POST[$variation_data_prefix][$i],
					'post_id' => $_POST['top-img-postid-' . $i]
				);
				update_post_meta( $_POST['product_id'], $_POST[$variation_data_prefix][$i] . '_top_img', $top_img_array );
			}
			if( $_POST['side-img-postid-' . $i] == 0 && trim($side_url_img_i) == ''){
				delete_post_meta( $_POST['product_id'], $_POST[$variation_data_prefix][$i] . '_side_img' );
			}else{
				$side_img_array = array(
					'idx' => $i,
					'variation_id', $variation_id,
					'img_url' => $_POST[$side_url_img_i],
					'variation_attribute' => $_POST[$variation_data_prefix][$i],
					'post_id' => $_POST['side-img-postid-' . $i]
				);
				update_post_meta( $_POST['product_id'], $_POST[$variation_data_prefix][$i] . '_side_img', $side_img_array );
			}
						
			$three_sixty_video_var = 'three-sixty-video-'.$i;
			$three_sixty_video_array = array(
				'url' => $_POST[$three_sixty_video_var]
			);
			update_post_meta( $_POST['product_id'], $_POST[$variation_data_prefix][$i] . '_three_sixty_video', $three_sixty_video_array );
		}
	}
	
	public function __construct(){
		$this->set_variations_prefix();
	}
	
}

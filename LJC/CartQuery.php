<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_CartQuery{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	/**
	 * Use to get cart items
	 * */
	public function get_cart_items(){
		$cart_items = LJC_CartEntity::get_instance()->get();
		if( isset($cart_items) && ! empty($cart_items['cart']) ){
			return $cart_items['cart'];
		}
		return false;
	}
	/**
	 * Use to get cart particularly settings items
	 * */
	public function get_cart_settings(){
		$setting_prefix = LJC_CartEntity::get_instance()->setting_cat;
		$cart_items = ljc_get_cart_items();
		$settings = false;
		if( isset($cart_items[$setting_prefix]) ){
			$settings = $cart_items[$setting_prefix];
		}
		return $settings;
	}
	/**
	 * Use to get cart particularly diamonds items
	 * */
	public function get_cart_diamonds(){
		$diamond_prefix = LJC_CartEntity::get_instance()->diamond_cat;
		$cart_items = $this->get_cart_items();
		$diamonds = false;
		if( isset($cart_items[$diamond_prefix]) ){
			$diamonds = $cart_items[$diamond_prefix];
		}
		return $diamonds;
	}
	/**
	 * get diamonds info with custom array data
	 * return object
	 * */
	public function cart_diamonds_info(){
		$diamonds = $this->get_cart_diamonds();
		$info = array();
		if( $diamonds && count($diamonds) > 0 ){
			foreach( $diamonds as $k => $v){
				$product = wc_get_product($v);
                if ( ! $product ) {
                    return false;
                }
				$info[] = array(
					'id' => $product->get_id(),
					'title' => $product->get_title(),
					'url' => $product->get_permalink()
				);
			}
			return json_decode(json_encode($info), FALSE);
		}
		return false;
	}
	/**
	 * wrap with wc_get_product output
	 * */
	public function wc_cart_diamonds_info(){
		$diamonds = $this->get_cart_diamonds();
		$info = array();
		if( $diamonds && count($diamonds) > 0 ){
			foreach( $diamonds as $k => $v){
				if( get_post_status( $v['product_id'] ) ){
					$product = wc_get_product($v['product_id']);
					$info[] = $product;
				}
			}
			return $info;
		}
		return false;
	}
	/**
	 * wrap with wc_get_product output
	 * */
	public function wc_cart_settings_info(){
		$settings = $this->get_cart_settings();
		$info = array();
		if( $settings && count($settings) > 0 ){
			$idx = 0;
			foreach( $settings as $k => $v){
				if( get_post_status( $v['product_id'] ) ){
					$price_variation = get_post_meta( $v['variation_id'], '_price', true);
					$product = wc_get_product($v['product_id']);
					$product_variation = LJC_CartEntity::get_instance()->get_variation_default_attribute_slug($product);
					$info[] = $product;
					$id = $v['product_id'];
					if( $product_variation ){
						$info[$idx]->variation = $product_variation;
						$info[$idx]->price_variation = $price_variation;
						$info[$idx]->get_variation = $product->get_available_variations();
						$id = $v['variation_id'];
					}
					$info[$idx]->setting_id = $id;
					$info[$idx]->product_id = $v['product_id'];
					$info[$idx]->variable_id = $v['variation_id'];
					if( isset($v['ring_size']) ){
						$info[$idx]->ring_size = $v['ring_size'];
					}
					$idx++;
				}
			}
			return $info;
		}
		return false;
	}
	public function cart_settings_info(){
		$settings = $this->get_cart_settings();
		$info = array();
		if( $settings && count($settings) > 0 ){
			foreach( $settings as $k => $v){
				if( get_post_status( $v['product_id'] ) ){
					$product = wc_get_product($v);
					if ( ! $product ) {
						return false;
					}
					$info[] = array(
						'id' => $product->get_id(),
						'title' => $product->get_title(),
						'url' => $product->get_permalink()
					);
				}
			}
			return json_decode(json_encode($info), FALSE);
		}
		return false;
	}
	
	public function saved_selections_info(){
		$loop = ljc_loop_saved_selection();
		$info_array = array();
		if( $loop ){
			$indx = 0;
			foreach($loop as $k => $v){
				$setting = array();
				$has_setting_post = false;
				if( get_post_status( $v['setting']['product_id'] ) ){
					$setting_price_variation = 0;
					$setting = wc_get_product($v['setting']['product_id']);
					$has_setting_post = true;
				}
				$diamond = array();
				if( get_post_status( $v['diamond'] ) ){
					$diamond = wc_get_product($v['diamond']);
				}
				//$setting_product_variation = LJC_CartEntity::get_instance()->get_variation_default_attribute_slug($v['setting']['product_id']);
				$info_array[] = array(
					'settings' => $setting,
					'diamond' => $diamond,
				);
				if( $has_setting_post ){
					if( isset($v['setting']['ring_size']) ){
						$info_array[$indx]['settings']->ring_size = $v['setting']['ring_size'];
					}
					//$variable_id = 0;
					if( $setting->product_type == 'variable' ){
						$setting_price_variation = get_post_meta( $v['setting']['variation_id'], '_price', true);
						$info_array[$indx]['settings']->price = $setting_price_variation;
						//$info_array[$indx]['settings']->variable_id = $v['setting']['variation_id'];
						$info_array[$indx]['settings']->setting_id = $v['setting']['variation_id'];
						$variable_id = $v['setting']['variation_id'];
						$info_array[$indx]['settings']->variable_id = $v['setting']['variation_id'];
					}else{
						$info_array[$indx]['settings']->variable_id = $v['setting']['product_id'];
						$info_array[$indx]['settings']->setting_id = $v['setting']['product_id'];
					}

					$parent_id = get_post($v['setting']['variation_id']);
					if( $parent_id->post_parent == 0 ){
						$info_array[$indx]['settings']->parent_id = $setting->id;
					}else{
						$info_array[$indx]['settings']->parent_id = $parent_id->post_parent;
					}
				}
				$indx++;
			}//foreach
			return $info_array;
		}//if
		return false;
	}
		
	public function __construct(){}
	
}//class

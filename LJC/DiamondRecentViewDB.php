<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_DiamondRecentViewDB{
	protected static $instance = null;
	private $option_name = 'diamond_recent_view_';
	private $option_name_created_at = 'diamond_recent_view_created_at_';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function show(){
	
	}
	
	public function get_option_name_recent_view(){
		$session_id = isset($_COKKIE['PHPSESSID']) ? $_COKKIE['PHPSESSID'] : LJC_Session::get_instance()->get_id();
		return $this->option_name . $session_id;
	}

	public function get_option_name_recent_view_created_at(){
		$session_id = isset($_COKKIE['PHPSESSID']) ? $_COKKIE['PHPSESSID'] : LJC_Session::get_instance()->get_id();
		return $this->option_name_created_at . $session_id;
	}
	
	public function get_data(){
		return $this->has_recent_view() ? get_option($this->get_option_name_recent_view()) : false;
	}
	
	public function has_recent_view(){
		return get_option($this->get_option_name_recent_view()) ? true:false;
	}
	
	public function count_recent_views(){
		if( $this->get_data() ){
			return count($this->get_data());
		}
		return 0;
	}
	
	public function init(){
		global $post;
		
		$array_id = array();
		
		if( !is_admin() && isset($post->ID) && is_product() ){
			$ljc_entity = new LJC_CartEntity;
			$is_diamond = $ljc_entity->is_product_cat($post->ID, $ljc_entity->get_cat_diamonds());
			if( $is_diamond ){
				if( $this->has_recent_view() ){
					$array_id = get_option($this->get_option_name_recent_view());
				}else{
					$array_id = array($post->ID);
					update_option($this->get_option_name_recent_view(), $array_id);
					update_option($this->get_option_name_recent_view_created_at(), date('Y-m-d H:i:s'));
				}
				if( !in_array($post->ID, $array_id ) ){
					array_push($array_id, $post->ID);
					update_option($this->get_option_name_recent_view(), $array_id);
				}
			}
		}
	}
	
	public function __construct(){}
	
}//class

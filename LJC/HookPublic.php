<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_HookPublic{
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function get_variations_attribute_name(){
		$config = mwp_config_ljc();
		return $config['woo_attribute_prefix']['pa_attribute'];
	}
	public function get_variations_attribute_prefix(){
		$config = mwp_config_ljc();
		return $config['woo_variations']['pa_prefix'];
	}
	
	public function ljc_woocommerce_thumbnail($attr, $attachment, $size){
		global $product, $woocommerce, $post;

		$pa_array = array();
		$top_img_key = '';
		$top_img_array = array();
		$side_img_key = '';
		$side_img_array = array();
		
		if( isset($_POST['action']) && 
			$_POST['action'] == 'prdctfltr_respond_550' &&
			isset($_POST['pf_request']) 
		){
			$prefix = $this->get_variations_attribute_prefix();
			$has_variant = false;
			foreach($_POST['pf_filters'] as $key => $val){
				if( isset($val[$prefix]) ){
					$top_img_key = $val[$prefix] . '_top_img';
					$side_img_key = $val[$prefix] . '_side_img';
					$has_variant = true;
				}
			}
			if( $has_variant ){
				$top_img_array = get_post_meta($post->ID, $top_img_key, true);
				$side_img_array = get_post_meta($post->ID, $side_img_key, true);
				if( isset($attr['class']) && $attr['class'] == 'hover-image' ){
					if( $side_img_array ){
						$attr['src'] = $side_img_array['img_url'];
						$srcset_side = wp_get_attachment_image_srcset($side_img_array['post_id'], 'shop_catalog' );
						$attr['srcset'] = $srcset_side;
					}
				}else{
					if( $top_img_array ){
						$attr['src'] = $top_img_array['img_url'];
						$srcset_top = wp_get_attachment_image_srcset($top_img_array['post_id'], 'shop_catalog' );
						$attr['srcset'] = $srcset_top;
					}
				}
			}
		}
		return $attr;
	}
	
	public function ljc_woo_custom_cart_button_text() {
		global $product;
		$ljc_entity = new LJC_CartEntity;
		$product_id = $product->id;
		$is_settings = $ljc_entity->is_product_cat($product_id, $ljc_entity->get_cat_settings());
		$is_diamond = $ljc_entity->is_product_cat($product_id, $ljc_entity->get_cat_diamonds());
		$text = __('Add to Cart', 'woocommerce');
		if($is_settings){
			$text = __( 'Choose This Setting', 'woocommerce' );
		}elseif($is_diamond){
			$text = __( 'Choose This Diamond', 'woocommerce' );
		}
		return $text;
	}
	
	public function ljc_action_woocommerce_after_shop_loop_item( ) { 
		global $product;
		$cat = strtolower(single_term_title('',false));
		$cat = str_replace(' ','-',$cat);
		$ljc_entity = new LJC_CartEntity;
		$is_settings = $ljc_entity->is_product_cat($product->id, $ljc_entity->get_cat_settings());
		$is_diamond = $ljc_entity->is_product_cat($product->id, $ljc_entity->get_cat_diamonds());
		if( is_single() && $is_settings || $is_diamond ){
			$add_to_cart_variation = '';
			if( in_the_loop() && !is_product() && $product->product_type == 'variable'){
				$variation = LJC_CartEntity::get_instance()->get_variation_default_attribute_slug($product);
				if( $variation ){
					$add_to_cart_variation = '&variation_id='.$variation->variation_id.'&'.$variation->attribute_key.'='.$variation->attribute_val.'';
				}
			}
			echo '<div>';
			echo '<a rel="nofollow" href="'.$product->add_to_cart_url().'?add-to-cart='.$product->id.$add_to_cart_variation.'" data-quantity="1" data-product_id="'.$product->id.'" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">'.$this->ljc_woo_custom_cart_button_text().'</a>';
			echo '</div>';
		}
	}
	
	public function __construct(){}	
	
}

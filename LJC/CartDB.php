<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_CartDB{
	private $pair_adding_to_woo_cart = false;
	private $ljc_session_cart = array();
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
		
	public function raw_delete_ljc(){
		global $wpdb;
		$wpdb->query("DELETE FROM $wpdb->options WHERE option_name like '%ljc_%'");
	}

	public function raw_get_ljc(){
		global $wpdb;
		$sql = "SELECT * FROM $wpdb->options WHERE option_name like '%ljc_%'";
		return $wpdb->get_results($sql);
	}
	
	public function set_option(){
		//$cart_sess = new WC_Session_Handler;
		$ljc_session_cart = $this->get_cart_object()->get_cart_array();
		$get_db = get_option($this->get_option_prefix());
		if( !$get_db ){
			update_option($this->get_option_prefix(), $ljc_session_cart);
		}
	}
			
	public function get_option_prefix(){
		$user_id = 	ljc_get_session_id();
		return 'ljc_cart_' . $user_id;
	}
	
	public function add_cart($key, $product_id, $variation_id = 0, $meta = array()){
		$get = $this->get();
		$cat_key = '';
		if( $get ){
			if( !isset($get['cart'][$key]) ){
				$cat_key = array($key);
			}else{
				$cat_key = $get['cart'][$key];
			}
			if( $variation_id == 0 ){
				$product = wc_get_product($product_id);
				$product_variation = LJC_CartEntity::get_instance()->get_variation_default_attribute_slug($product);
				if( $product_variation ){
					$variation_id = $product_variation->variation_id;
				}
			}
			$pid_vid = $product_id . $variation_id;
			if( !in_array($pid_vid, array_keys($cat_key)) ){
				$get['cart'][$key][$pid_vid]['product_id'] = $product_id;
				$get['cart'][$key][$pid_vid]['variation_id'] = $variation_id;
				if( isset($meta['ring_size']) ){
					$get['cart'][$key][$pid_vid]['ring_size'] = $meta['ring_size'];
				}
				update_option($this->get_option_prefix(), $get);
			}
		}
	}	
	
	public function get(){
		return get_option($this->get_option_prefix());
	}

	public function has_cart_items(){
		$get = $this->get();
		if( $get && (isset($get['cart']) && count($get['cart'])) > 0 ){
			return true;
		}
		return false;
	}
	
	public function clean(){
		update_option($this->get_option_prefix(),'');
	}

	public function remove_db(){
		return delete_option($this->get_option_prefix());
	}
	
	public function has_setting(){
		$get = $this->get();
		if( $get ){
			if( isset($get['cart']['settings']) && count($get['cart']['settings']) >= 1 ){
				return true;
			}
		}
		return false;
	}

	public function has_diamond(){
		$get = $this->get();
		if( $get ){
			if( isset($get['cart']['diamonds']) && count($get['cart']['diamonds']) >= 1 ){
				return true;
			}
		}
		return false;
	}
	
	public function remove_setting($id){
		$get = $this->get();
		if( $get ){
			if( $get && isset($get['cart']['settings']) ){
				$id = null;
				$key = null;
				foreach($get['cart']['settings'] as $k => $v){
					if( $_POST['setting_id'] == $v['variation_id'] ){
						$id = $v['product_id'];
						$key = $k;
					}elseif( $_POST['setting_id'] == $v['product_id'] ){
						$id = $_POST['setting_id'];
						$key = $k;
					}
				}

				if( !is_null($key) ){
					unset($get['cart']['settings'][$key]);
					if(empty($get['cart']['settings'])) {
                        unset($get['cart']['settings']);
                    }
                    return update_option($this->get_option_prefix(), $get);
				}
			}
			return false;
		}
		return false;
	}
	public function remove_diamond($id){
		$get = $this->get();
		if( $get ){
			if( $get && isset($get['cart']['diamonds']) ){
				$id = null;
				$key = null;
				foreach($get['cart']['diamonds'] as $k => $v){
					if( $_POST['diamond_id'] == $v['variation_id'] ){
						$id = $v['product_id'];
						$key = $k;
					}elseif( $_POST['diamond_id'] == $v['product_id'] ){
						$id = $v['diamond_id'];
						$key = $k;
					}
				}
				if( !is_null($key) ){
					unset($get['cart']['diamonds'][$key]);
					if(empty($get['cart']['diamonds'])) {
                        unset($get['cart']['diamonds']);
                    }
                    return update_option($this->get_option_prefix(), $get);
				}
			}
			return false;
		}
		return false;
	}
	
	public function set_has_added_to_cart($added = false){
		$cart = $this->get_cart_object();
		$get = $this->get();
		if( $get ){
			$get['has_added_to_cart'] = $added;
			update_option($this->get_option_prefix(), $get);
		}
	}
	
	public function get_has_added_to_cart(){
		$cart = $this->get_cart_object();
		$get = $this->get();
		if( $get && isset($get['has_added_to_cart']) ){
			return $get['has_added_to_cart'];
		}
		return $cart->get_has_added_to_cart();
	}
	
	public function set_pair_adding_to_woo_cart($set = false){
		$this->pair_adding_to_woo_cart = $set;
	}

	public function get_pair_adding_to_woo_cart(){
		return $this->pair_adding_to_woo_cart;
	}
	
	public function get_cart_object(){
		return LJC_Cart::get_instance();
	}
	
	public function __construct(){
		$this->set_option();
		$this->set_pair_adding_to_woo_cart();
	}
	
}//class

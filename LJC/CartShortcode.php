<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_CartShortcode{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function init_shortcode(){
		$settings_data = ljc_get_cart_settings_data();
		$settings = ljc_wc_cart_settings_info();
		$diamonds_data = ljc_get_cart_diamonds_data();
		$diamonds = ljc_wc_cart_diamond_info();
		$get_cart = ljc_get_data();
		$saved_selections = ljc_query_saved_selection();
		$saved_selections_template_part = ljc_public_partials() .'cart/ljc-save-selection-part.php';
		$get =  get_option(LJC_CartDB::get_instance()->get_option_prefix());
		ob_start();
		require_once( ljc_public_partials() .'cart/ljc-shortcode-cart.php' );
		$output = ob_get_clean();
		return $output;
	}
	
	public function __construct(){
		add_shortcode('ljc_temp_cart', array($this,'init_shortcode'));
	}
	
}//class

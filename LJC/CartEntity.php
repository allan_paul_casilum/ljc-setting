<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_CartEntity{
	public $setting_cat;
	public $diamond_cat;
	public $entity_cart = null;
	public $obj_cart = null;
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function get_cat_settings(){
		return $this->setting_cat;
	}

	public function get_cat_diamonds(){
		return $this->diamond_cat;
	}
	
	public function set_cat_settings(){
		$this->setting_cat = ljc_get_cat_setting();
	}

	public function set_cat_diamonds(){
		$this->diamond_cat = ljc_get_cat_diamond();
	}
	
	/**
	 * Check product category per id
	 * @var int $product_id	check the ID
	 * @var	string $check_this_cat check the category slug
	 * */
	public function is_product_cat($product_id, $check_this_cat){
		if( has_term($check_this_cat, 'product_cat', $product_id) ){
			return true;
		}
		return false;
	}
	
	public function add($product_id, $variation_id = 0, $meta = array()){
		if( $this->is_product_cat($product_id, $this->setting_cat) ){
			$this->get_current_entity_class()->add_cart($this->setting_cat, $product_id, $variation_id, $meta);
		}elseif($this->is_product_cat($product_id, $this->diamond_cat)){
			$this->get_current_entity_class()->add_cart($this->diamond_cat, $product_id, $variation_id, $meta);
		}
		return false;
	}
	
	public function get(){
		return $this->get_current_entity_class()->get();
	}
	
	public function is_finalize(){
		return $this->get_current_entity_class()->is_finalize();
	}
	
	public function clean(){
		return $this->get_current_entity_class()->clean();
	}
	
	public function get_current_entity_class(){
		return LJC_CartDB::get_instance();
	}

	public function get_current_cart_class(){
		return LJC_Cart::get_instance();
	}
	
	public function ljc_template_redirect(){
		if( $_POST && isset( $_POST['empty_saved_selections'] ) && $_SERVER['REQUEST_METHOD'] == "POST" ) {
			LJC_SaveCartDB::get_instance()->remove_db();
		}
		if( $_POST && isset( $_POST['empty_selections'] ) && $_SERVER['REQUEST_METHOD'] == "POST" ) {
			LJC_CartDB::get_instance()->remove_db();
		}
	}
	
	public function get_variation_default_attribute_slug($product, $output = 'obj'){
		$config = mwp_config_ljc();
		$attribute_prefix = $config['woo_variations']['pa_prefix'];
		$default_attribute = array();
		$get_key = array();
		$metal = array(
			'variation_id' => 0,
			'attribute_val' => '',
			'attribute_key' => '',
		);
		if( $product instanceof WC_Product && $product->product_type == 'variable' ){//if instance of
			if( $product->get_variation_default_attributes() ){
				$default_attribute = $product->get_variation_default_attributes();
				$get_key = array_keys($default_attribute);
			}
			if( in_array($attribute_prefix, $get_key) ){//in array
				$attribute_slug = $default_attribute[$attribute_prefix];
				$attribute_key = $attribute_prefix;
				if( $product->get_available_variations() ) {//get_available_varations
					$_attribute_key = 'attribute_'.$attribute_key;
					$metal = array(
						'variation_id' => 0,
						'attribute_val' => '',
						'attribute_key' => $_attribute_key,
					);
					foreach($product->get_available_variations() as $k => $v){
						if( $attribute_slug == $v['attributes']['attribute_'.$attribute_key] ){
							$metal['variation_id'] = $v['variation_id'];
							$metal['attribute_val'] =  $v['attributes']['attribute_'.$attribute_key];
							$metal['attribute_data'] =  $v;
						}
					}
					switch($output){
						case 'arr':
							return $metal;
						case 'obj':
						default:
							return json_decode (json_encode ($metal), FALSE);
						break;
					}
				}//get_available_varations
				return false;
			}//in array
			return false;
		}//if instance of
		return false;
	}
	
	public function __construct(){
		//actually sets
		$this->set_cat_settings();
		$this->set_cat_diamonds();
	}
	
}//class

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_WooTemplate{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	//add the breadcrumb steps
	public function woocommerce_before_main_content(){
		require_once( ljc_public_partials() .'breadcrumb-steps.php' );
	}
	
	public function avada_before_body_content(){
		global $post, $wp;
		$queryString = $_SERVER['QUERY_STRING'];
		$config = mwp_config_ljc();
		$verbage = $config['verbage'];
		$sub_heading = '';
		if( is_product_category(array('settings')) ){
			$sub_heading = $verbage['subheading_settings'];
		}
		if( is_product_category(array('diamonds')) ){
			$sub_heading = $verbage['subheading_diamonds'];
		}
		$engagement_array = array(
			'vintage-rings',
			'lj-vintage-inspired-rings',
			'wedding-rings',
			'for-him',
			'for-her',
			'for-him-more-jewelry',
			'rings',
			'for-her',
			'for-her-rings-2',
			
		);
		if( is_product_category($engagement_array) ){
			$sub_heading = single_cat_title('', false);
		}
		if( is_product_category('engagement-rings') ){
			$sub_heading = $verbage['engagement_cat'];
		}
		
		if ($queryString == "pa_for=for-him") {
			$sub_heading = "For Him";
		} else if ($queryString == "pa_for=for-her") {
			$sub_heading = "For Her";
		} else if ($queryString == "pa_new-or-vintage=Vintage") {
			$sub_heading = "Vintage Rings";
		} else if ($queryString == "pa_new-or-vintage=Replica") {
			$sub_heading = "Vintage Inspired Rings";
		}
		

		//$class = apply_filters('ljc_subheading_class', '');
		//echo '<h2 class="'.$class.'" id="ljc_subheading">'.$sub_heading.'</h2>';
        echo '<div id="ljc_subheading">'.$sub_heading.'</div>';
	}
	
	public function wc_remove_all_quantity_fields($return, $product){
		if( is_product() ){
			$cart_entity = new LJC_CartEntity;
			$setting_cat = $cart_entity->is_product_cat($product->id, $cart_entity->setting_cat);
			$diamond_cat = $cart_entity->is_product_cat($product->id, $cart_entity->diamond_cat);
			$custom_rings = has_term('custom-rings', 'product_cat', $product->id);
			if( $setting_cat || $diamond_cat || $custom_rings ){
				return true;
			}
		}
	}
	
	public function wc_add_to_cart_message($message){
		$id = $_POST['add-to-cart'];
		$cart_entity = new LJC_CartEntity;
		$setting_cat = $cart_entity->is_product_cat($id, $cart_entity->setting_cat);
		$diamond_cat = $cart_entity->is_product_cat($id, $cart_entity->diamond_cat);
		$custom_rings = has_term('custom-rings', 'product_cat', $id);
		if( $setting_cat || $diamond_cat || $custom_rings ){
			//$message = '';
			$title = get_the_title( $id );
			$message = $title . __( ' Successfully Chosen ', 'woocommerce' );
		}
		return $message;
	}
	public function woo_reorder_tabs( $tabs) {

		$tabs['additional_information']['priority'] = 5;
		$tabs['description']['priority'] = 10;
		$tabs['reviews']['priority'] = 15;

		return $tabs;
	}
	
	public function woo_rename_tabs( $tabs ) {

		$tabs['description']['title'] = __( 'Additional Information' );		// Rename the description tab
		$tabs['reviews']['title'] = __( 'Ratings' );				// Rename the reviews tab
		$tabs['additional_information']['title'] = __( 'Description' );	// Rename the additional information tab

		return $tabs;

	}
	
	public function ljc_product_description_heading() {
		return __('Additional Information', 'woocommerce');
	}
	
	public function ljc_product_additional_information_heading() {
		return __('Product Description', 'woocommerce');
	}
}

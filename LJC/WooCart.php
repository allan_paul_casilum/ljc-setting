<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_WooCart{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function woo_custom_add_to_cart($cart_item_data, $product_id, $variation_id){
		global $woocommerce, $wp_session;
		$attr = mwp_config_ljc();
		$ljc_entity = new LJC_CartEntity;
		$is_settings = $ljc_entity->is_product_cat($product_id, $ljc_entity->get_cat_settings());
		$is_diamond = $ljc_entity->is_product_cat($product_id, $ljc_entity->get_cat_diamonds());
		$ring_size = 0;
		if(isset($_POST['attribute_' . $attr['ring_size']['pa_prefix']])){
			$ring_size = $_POST['attribute_' . $attr['ring_size']['pa_prefix']];
		}
		$meta = array(
			'ring_size' => $ring_size
		);
		LJC_CartEntity::get_instance()->add($product_id, $variation_id, $meta);
		//do not redirect if doing ajax
		if ( !is_admin() 
			&& ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
			&& ($is_settings ||  $is_diamond )
		){
			//if in setting page
			//and has no diamond
			//redirect to step 2
			if( $is_settings && !ljc_has_diamond() ){
				$diamond_url = get_term_link($ljc_entity->get_cat_diamonds(),'product_cat');
				wp_redirect($diamond_url);
				exit;
			}
			//if in diamond page
			//and has no setting
			//redirect to step 1
			if( $is_diamond && !ljc_has_setting() ){
				$setting_url = get_term_link($ljc_entity->get_cat_settings(),'product_cat');
				wp_redirect($setting_url);
				exit;
			}
			//if has setting and diamond
			//redirect to complete your ring
			if( ljc_has_diamond() && ljc_has_setting() ){
				wp_redirect($attr['url']['complete_your_ring']);
				exit;
			}
		}
		return $cart_item_data;
	}
	
	public function woocommerce_cart_item_removed(){
		if( WC()->cart->get_cart_contents_count() == 0 ){
			LJC_CartDB::get_instance()->set_has_added_to_cart(false);
		}
	}
	
	//this should run under WC hook
	public function exclude_setting_diamond_product($product_id){
		$ljc_entity = new LJC_CartEntity;
		$is_settings = $ljc_entity->is_product_cat($product_id, $ljc_entity->get_cat_settings());
		$is_diamond = $ljc_entity->is_product_cat($product_id, $ljc_entity->get_cat_diamonds());
		if( $is_settings || $is_diamond ){
			$ljc_attribute = array();
			foreach($_POST as $key => $value) {
				if (strpos($key, 'attribute_') === 0) {
					// value starts with book_
					$ljc_attribute[$key] = $value;
				}
			}
			$variation_id = 0;
			if( isset($_POST['variation_id']) ){
				$variation_id = $_POST['variation_id'];
			}
			$prod_unique_id = WC()->cart->generate_cart_id( $product_id, $variation_id, $ljc_attribute );
			unset( WC()->cart->cart_contents[$prod_unique_id] );
		}
	}
	
	public function woocommerce_add_to_cart(){
		//if( !LJC_CartDB::get_instance()->get_pair_adding_to_woo_cart() ){
			if( isset($_POST['add-to-cart']) ){
				$id = $_POST['add-to-cart'];
			}else{
				$id = $_POST['product_id'];
			}
			$this->exclude_setting_diamond_product($id);
		//}
	}
	
	public function woocommerce_check_cart_items(){
		global $woocommerce;
		// Check the cart for this product
		/*if( !LJC_CartDB::get_instance()->get_has_added_to_cart() ){
			WC()->cart->empty_cart( true );
		}*/
	}
	
	public function woocommerce_checkout_order_processed(){
		LJC_Cart::get_instance()->delete_db();
	}

	public function woocommerce_cart_loaded_from_session(){
		$this->woocommerce_check_cart_items();
	}
	
	public function ajax_ljc_add_to_cart(){
		$msg = '';
		$data = $_POST;
		$status = 0;
		$woo_cart_url = '';
		//add to cart using WOO api
		$setting_id = 0;
		if( isset($_POST['setting_id']) ){
			$setting_id = $_POST['setting_id'];
		}
		$setting_variation_id = 0;
		if( isset($_POST['setting_variation_id']) ){
			$setting_variation_id = $_POST['setting_variation_id'];
		}
		$setting_ring_size = 0;
		if( isset($_POST['setting_ring_size']) ){
			$setting_ring_size = $_POST['setting_ring_size'];
		}
		$diamond_id = 0;
		if( isset($_POST['diamond_id']) ){
			$diamond_id = $_POST['diamond_id'];
		}
		
		//ticket to send pair to cart
		//LJC_CartDB::get_instance()->set_pair_adding_to_woo_cart(true);
		$product_Id = ljc_create_custom_ring($setting_id, $diamond_id, $setting_ring_size);
		if( $product_Id ){
			$data['custom_ring_product_id'] = $product_Id;
			//WC()->cart->add_to_cart( $setting_id, 1, $setting_variation_id );
			WC()->cart->add_to_cart( $product_Id );
			$msg = 'added to custom ring';
			$status = 1;
			$woo_cart_url = WC()->cart->get_cart_url();
		}else{
			$msg = 'not added to custom ring';
		}
		//set to false so no individual setting and diamond is added
		//LJC_CartDB::get_instance()->set_pair_adding_to_woo_cart(false);
		echo json_encode(
			array(
				'msg'=>$msg,
				'data'=>$data,
				'status'=>$status,
				'woo_cart_url' => $woo_cart_url
			)
		);
		die();
	}
	
}

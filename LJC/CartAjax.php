<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_CartAjax{
	private $ljc_session_cart;
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function ajax_ljc_remove_setting(){
		$res = array(
			'msg'=>'fail',
			'status'=>0,
		);
		if( isset($_POST['setting_id']) ){
			$setting_id = $_POST['setting_id'];
			$db = LJC_CartDB::get_instance()->remove_setting($setting_id);
		}
		if( $db ){
			$res['msg'] = 'succes';
			$res['status'] = 1;
		}
		echo json_encode($res);
		die();
	}

	public function ajax_ljc_remove_diamond(){
		$res = array(
			'msg'=>'fail',
			'status'=>0,
		);
		if( isset($_POST['diamond_id']) ){
			$diamond_id = $_POST['diamond_id'];
			$db = LJC_CartDB::get_instance()->remove_diamond($diamond_id);
		}
		if( $db ){
			$res['msg'] = 'succes';
			$res['status'] = 1;
		}
		echo json_encode($res);
		die();
	}
	
	public function ajax_ljc_reset_selection(){
		$res = array(
			'msg'=>'fail',
			'status'=>0,
		);
		$del = LJC_CartDB::get_instance()->remove_db();
		if( $del ){
			$res['msg'] = 'succes';
			$res['status'] = 1;
		}
		echo json_encode($res);
		die();
	}
	
}

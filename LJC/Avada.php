<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LJC_Avada{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function the_title($title, $id){
		global $post;
		$config = mwp_config_ljc();
		$verbage = $config['verbage'];
		$title_heading = $config['title_heading'];
        $cart_entity = new LJC_CartEntity;
        
        $engagement_array = $title_heading['engagement_array'];
        $wedding_ring = $title_heading['wedding_ring'];
        $jewerly_ring = $title_heading['jewerly_ring'];
        
        $array_cat = array_merge($engagement_array, $wedding_ring, $jewerly_ring);
        
        //if in the product category page
        if( is_product_category() ){
			if( is_product_category(array('settings','diamonds')) ){
				$title = $verbage['settings_product'];
			}
			
			if( is_product_category($engagement_array) ){
				$title = $verbage['engagement_product'];
			}
			
			if( is_product_category($wedding_ring) ){
				$title = $verbage['wedding_product'];
			}
			
			if( is_product_category($jewerly_ring) ){
				$title = $verbage['jewerly_product'];
			}
		}elseif( is_product() ){
			//single product
			$setting_cat = $cart_entity->is_product_cat($id, $cart_entity->setting_cat);
			$diamond_cat = $cart_entity->is_product_cat($id, $cart_entity->diamond_cat);
            if( $setting_cat ){
                $title = $verbage['setting_single'];
            }
            if( $diamond_cat ){
                $title = $verbage['diamond_single'];
            }
            foreach($array_cat as $key => $val){
				if( has_term($val, 'product_cat', $id) ){
					$title = $val;
				}
			}
		}

        return $title;
	}
	
	public function fusion_breadcrumbs_defaults($defaults){
		//$defaults = array();
		//return $defaults;
		//print_r($defaults);
	}
	
}

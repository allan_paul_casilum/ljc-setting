<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
//path
function ljc_config($key){
	$config = mwp_config_ljc();
	if( isset($config[$key]) ){
		return $config[$key];
	}
	return false;
}
function ljc_plugin_path($key){
	$path = ljc_config('plugin_path');
	if( isset($path[$key]) ){
		return $path[$key];
	}
	return false;
}
function ljc_public_partials(){
	return ljc_plugin_path('public_partials');
}
function ljc_admin_partials(){
	return ljc_plugin_path('admin_dir_partials');
}
function ljc_verbage($key, $echo = true){
	$config = ljc_config('verbage');
	if( isset($config[$key]) ){
		if( $echo ){
			echo $config[$key];
		}else{
			return $config[$key];
		}
	}
	return false;
}
function ljc_localize(){
	return ljc_config('localize');
}
function get_current_post(){
	global $post;
	return $post;
}

function ljc_diamonds_container_datatables($data = null){
	if( !is_null($data) && is_product_category( 'diamonds' ) ){
		$path = ljc_public_partials() . '/e3ve-datatables.html';
		require_once($path);
	}
	return false;
}

function ljc_diamonds_loop_datatables($data = null){
	if( !is_null($data) && is_product_category( 'diamonds' ) ){
		$path = ljc_public_partials() . '/diamond_datatables.php';
		require_once($path);
	}
	return false;
}

function ljc_diamond_recentreview_loop_datatables(){
	$recent_view_db_array = LJC_DiamondRecentViewDB::get_instance()->get_data();
	if( $recent_view_db_array ){
		$array_query = array( 
			'post_type' => 'product', 
			'post__in' => $recent_view_db_array 
		);
		$recent_view_wp_query = new WP_Query($array_query);
		$path = ljc_public_partials() . '/diamond_datatables_recentreview.php';
		require_once($path);
	}
}

function ljc_diamond_compare_datatables(){
	$comparison_db_array = LJC_DiamondComparisonDB::get_instance()->get_data();
	if( $comparison_db_array ){
		$array_query = array( 
			'post_type' => 'product', 
			'post__in' => $comparison_db_array 
		);
		$comparison_wp_query = new WP_Query($array_query);
		$path = ljc_public_partials() . '/diamond_datatables_compare.php';
		require_once($path);
	}
}
function ljc_diamond_compare_count(){
	return LJC_DiamondComparisonDB::get_instance()->count();
}
function ljc_get_diamond_compare_db(){
	if( LJC_DiamondComparisonDB::get_instance()->has_db() ){
		return LJC_DiamondComparisonDB::get_instance()->get_data();
	}
	return array();
}
/**
 * CJ, this should be added in the import of diamond
 * not a good practice to loop this to get the diamond image on external site
 * */
function ljc_get_og_img($url){
	$meta_og_img = '';
	if( trim($url) != '' ){
		$sites_html = file_get_contents($url);
		$html = new DOMDocument();
		@$html->loadHTML($sites_html);
		$meta_og_img = null;
		//Get all meta tags and loop through them.
		foreach($html->getElementsByTagName('meta') as $meta) {
			//If the property attribute of the meta tag is og:image
			if($meta->getAttribute('property')=='og:image'){ 
				//Assign the value from content attribute to $meta_og_img
				$meta_og_img = $meta->getAttribute('content');
			}
		}
	}
	return $meta_og_img;
}

require plugin_dir_path( __FILE__ ) . 'cart-functions.php';

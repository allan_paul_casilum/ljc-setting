<table id="loopRecentViewDatatables" class="display compact" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Compare</th>
			<th>Shape</th>
			<th>Carat</th>
			<th>Color</th>
			<th>Clarity</th>
			<th>Cut</th>
			<th>Report</th>
			<th>Price</th>
			<th>View</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th>Compare</th>
			<th>Shape</th>
			<th>Carat</th>
			<th>Color</th>
			<th>Clarity</th>
			<th>Cut</th>
			<th>Report</th>
			<th>Price</th>
			<th>View</th>
		</tr>
	</tfoot>
	<tbody>
		<?php while ( $recent_view_wp_query->have_posts() ) : $recent_view_wp_query->the_post();
			wc_get_template_part( 'content', 'diamond-product' );
		endwhile;$recent_view_wp_query->reset_postdata(); ?>
	</tbody>
</table>

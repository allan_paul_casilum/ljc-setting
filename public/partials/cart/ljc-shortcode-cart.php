<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>
<div class="bootstrap-ljc ljc-temp-cart">
	<div class="temp-cart-header">
		<div id="ljc_subheading"><?php ljc_verbage('cart_title'); ?></div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="ljc-settings">
				<h2><?php ljc_verbage('setting_name'); ?></h2>
				<?php if( $settings ){//if settings ?>
						<select name="dropdown-settings" id="dropdown-settings">
							<?php foreach($settings as $k => $v){//foreach settings ?>
									<?php
										$price = '';
										if( isset($v->variation) ){
											$price = $v->price_variation;
										}else{
											$price = $v->get_price();
										}
									?>
									<?php
										$data_setting_variation_id = 'data-setting-variation-id="'.$v->variable_id.'" ';
										$data_setting_ring_size = 'data-setting-ring-size="'.$v->ring_size.'" ';
									?>
									<option value="<?php echo $v->setting_id;?>" <?php echo $data_setting_ring_size;?> <?php echo $data_setting_variation_id;?>><?php echo $v->get_title();?> - <?php echo wc_price($price); ?></option>
							<?php }//foreach settings ?>
						</select>
						<button class="remove-setting-selection"><?php ljc_verbage('remove_this_setting_item');?></button>
				<?php }else{//if settings ?>
						<h3><?php ljc_verbage('no_setting_selected'); ?></h3>
				<?php }//if settings ?>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="ljc-diamond">
				<h2><?php ljc_verbage('diamond_name'); ?></h2>
				<?php if( $diamonds ){//if settings ?>
						<select name="dropdown-diamond" id="dropdown-diamond">
							<?php foreach($diamonds as $k => $v){//foreach diamonds ?>
									<option value="<?php echo $v->get_id();?>"><?php echo $v->get_title();?> - <?php echo wc_price($v->get_price());?></option>
							<?php }//foreach diamonds ?>
						</select>
						<button class="remove-diamond-selection"><?php ljc_verbage('remove_this_diamond_item');?></button>
				<?php }else{//if diamonds ?>
						<h3><?php ljc_verbage('no_diamond_selected'); ?></h3>
				<?php }//if diamonds ?>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="dropdown-selection-msg"></div>
		</div>
		<div class="col-sm-12">
			<br>
			<hr>
			<?php if( $settings && $diamonds ){//if  ?>
			<div class="save-add-button">
                <div class="add-selection-msg"></div>
				<button class="add-selection "><?php ljc_verbage('saved_selection');?></button> OR
				<button class="add-to-cart-selection"><?php ljc_verbage('add_to_cart_selection');?></button>
				<br>
				<br>
			</div>
			<?php }//if ?>
			<?php if( $settings || $diamonds ){//if  ?>
				<form name="empty-selections" method="post" action="<?php //echo get_permalink( get_current_post()->ID ); ?>">
					<input type="submit" class="" name="empty_selections" value="<?php ljc_verbage('empty_selection');?>">
				</form>
			<hr>
			<?php }//if ?>
			<br>
			<div class="save-selection-content">
				<?php require_once $saved_selections_template_part; ?>
				
			</div>
		</div>
	</div>
</div>

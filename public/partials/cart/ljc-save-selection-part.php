<h3><?php ljc_verbage('current_save_selections');?></h3>
<div class="row">
	<?php if( $saved_selections ) { //if $saved_selections ?>
		<?php foreach($saved_selections as $k => $v){//foreach saved_selections ?>
				<div class="col-md-6 col-sm-12">
					<?php if( is_array($v) ){//if is array ?>
							<?php $data_attribute_id = ''; ?>
							<?php $data_setting_variation_id = ''; ?>
							<?php $data_setting_ring_size = ''; ?>
							<?php $total = 0; ?>
							<?php foreach($v as $key => $val){//for each items ?>
									<?php if( isset($val->id) && get_post_status( $val->id ) ){ ?>
										<?php if( isset($val->price) ){ ?>
											<?php $total += $val->price; ?>
										<?php }else{ ?>
											<?php $total += $val->get_price(); ?>
										<?php } ?>
										<div class="media loop-idx-<?php echo $k;?>">
										  <div class="media-left" style="float:left;">
											<a href="<?php echo $val->get_permalink(); ?>">
											  <?php echo $val->get_image();?> 
											</a>
										  </div>
										  <div class="media-body">
											<a href="<?php echo $val->get_permalink(); ?>">
												<h4 class="media-heading">
													<?php echo $val->get_title(); ?>
												</h4>
											</a>
											<?php ljc_verbage('temp_cart_total_loop_price');?> : 
											<?php if( isset($val->price) ){ ?>
												<span class="price"><span class="woocommerce-Price-amount amount"><?php echo wc_price($val->price); ?></span></span>
											<?php }else{ ?>
												<span class="price"><span class="woocommerce-Price-amount amount"><?php echo $val->get_price_html(); ?></span></span>
											<?php } ?>
											
											<?php
												if( $key == 'settings' ){
													$data_attribute_id .= 'data-setting-id="'.$val->setting_id.'" ';
													$data_setting_variation_id .= 'data-setting-variation-id="'.$val->variable_id.'" ';
													$data_setting_ring_size .= 'data-setting-ring-size="'.$val->ring_size.'" ';
												}elseif( $key == 'diamond' ){
													$data_attribute_id .= 'data-diamond-id="'.$val->get_id().'"';
												}
											?>
										  </div>
										</div>
									<?php }//get_post_status ?>
							<?php }//for each items ?>
					<?php }//if is_array ?>
					<p></p>
					<p><?php ljc_verbage('temp_cart_total');?> : <span class="price"><span class="woocommerce-Price-amount amount"><?php echo wc_price($total); ?></span></p>
					<button class="add-to-cart-selection" <?php echo $data_attribute_id;?> <?php echo $data_setting_variation_id;?> <?php echo $data_setting_ring_size;?> data-idx="<?php echo $k;?>" ><?php ljc_verbage('temp_cart_add_to_cart_button');?></button>
					<button class="remove-to-cart-selection" <?php echo $data_attribute_id;?> data-idx="<?php echo $k;?>"><?php ljc_verbage('temp_cart_remove_to_cart_button');?></button>
					<div class="loop-cart-ajax-msg-<?php echo $k;?>"></div>
					<hr>
				</div>
		<?php }//foreach saved_selections ?>
		<div class="col-sm-12 empty-saved-selections-container">
			<form name="empty-save-selections" method="post" action="">
				<input type="submit" class="" name="empty_saved_selections" value="<?php ljc_verbage('empty_saved_selection');?>">
			</form>
		</div>
	<?php } //if $saved_selections ?>
</div>
				

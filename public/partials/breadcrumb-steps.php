<?php

/*####################
 * Get URL info
 */###################
$objSettingsCategory        = get_product_category_by_slug('settings');
$objDiamondsCategory        = get_product_category_by_slug('diamonds');

$intSettingsCatId           = $objSettingsCategory->term_id;
$intDiamondsCatId           = $objDiamondsCategory->term_id;
$intCustomRingsCatId        = get_product_cat_ID('Custom Rings');

$arySettingsUrl             = parse_url( get_category_link($intSettingsCatId) );
$aryDiamondsUrl             = parse_url( get_category_link($intDiamondsCatId) );
$aryCustomRingsUrl          = parse_url( get_category_link($intCustomRingsCatId) );

$strSettingsUrl             = $arySettingsUrl['path'];          //'/product-category/settings/'
$strDiamondsUrl             = $aryDiamondsUrl['path'];          //'/product-category/diamonds/'
$strCustomRingsUrl          = $aryCustomRingsUrl['path'];       //'/product-category/custom-rings/'
$strCompleteUrl             = '/complete-your-ring/';
$strCompleteUrlModal        = '/complete-your-ring-modal/';
$strFullCompleteUrl         = home_url($strCompleteUrlModal);
//var_dump('$strFullCompleteUrl',$strFullCompleteUrl);
$intSettingsUrlLen          = strlen($strSettingsUrl);          //27
$intDiamondsUrlLen          = strlen($strDiamondsUrl);          //27
$intCompleteUrlLen          = strlen($strCompleteUrl);          //20

$strThisUri                 = $_SERVER['REQUEST_URI'];
$aryThisUrl                 = parse_url($strThisUri);
//$bol = in_category('diamonds');
//var_dump('$bol',$bol);
/*####################
 * Set preliminary vars
 */###################
$strActiveCrumbClass        = ' orange-active';
$strNoDisplayCss            = ' style="display: none;"';
$strLongSettingText         = 'Choose<br />Your<br />Setting';
$strLongDiamondText         = 'Choose<br />Your<br />Diamond';
$strCompleteText            = 'Complete Your Ring';
$strSetting                 = 'setting';
$strDiamond                 = 'diamond';
$strSettingsCategorySlug    = 'settings';
$strDiamondsCategorySlug    = 'diamonds';
$strCustomRingsCategorySlug = 'custom-rings';
$strStep1                   = $strSetting;
$strStep2                   = $strDiamond;
$strShortSettingText        = 'Setting';
$strShortDiamondText        = 'Diamond';
$strActiveCompleteText      = 'Complete Your Ring';
$strStep1Text               = $strLongSettingText;
$strStep2Text               = $strLongDiamondText;
$strStep1WrapperClass       = '';
$strStep2WrapperClass       = '';
$strStep3WrapperClass       = '';
$strStep1ViewLink           =
$strViewSettingLink         =
$strStep2ViewLink           =
$strViewDiamondLink         = '#';
$strStep1ChangeLink         =
$strChangeSettingLink       = $strSettingsUrl;
$strStep2ChangeLink         =
$strChangeDiamondLink       = $strDiamondsUrl;
$strStep1PriceCss           =
$strSettingPriceCss         =
$strStep1LinkCss            =
$strSettingLinkCss          =
$strStep2PriceCss           =
$strDiamondPriceCss         =
$strStep2LinkCss            =
$strDiamondLinkCss          =
$strStep3PriceCss           = $strNoDisplayCss;
$intStep1Price              = 0;
$intHiddenSettingPrice      = 0;
$intStep2Price              = 0;
$intHiddenDiamondPrice      = 0;
$intStep3Price              = 0;
$bolIsBuilding              = false;
$bolIsSettingProductPage    = false;
$bolIsDiamondProductPage    = false;
$bolIsCustomRingProductPage = false;
$bolIsSettingsCategoryPage  = false;
$bolIsDiamondsCategoryPage  = false;
$bolIsCompleteUri           = ( (substr($strThisUri, 0, $intCompleteUrlLen) == $strCompleteUrl) ) ? true : false;
$bolSettingIsFirst          = true;
$bolDiamondIsFirst          = false;
$bolShowCrumbs              = false;
$bolSettingSelected         = false;
$bolDiamondSelected         = false;
$bolSettingExists           = false;
$bolDiamondExists           = false;
$aryActiveSettingIds        = array();
$aryActiveDiamondIds        = array();
$objActiveSetting           = null;
$objActiveDiamond           = null;
$intSettingId               = null;
$intSettingParentId         = null;
$intDiamondId               = null;
$intDiamondParentId         = null;
$intSettingCount            = 0;
$intDiamondCount            = 0;
$intItemsCount              = 0;
$intSelectionCount          = 0;
$strSelectionCountText      = '';

/*####################
 * Get current page object and any hidden cart objects
 */###################
$objThis = get_queried_object();
$aryHiddenCartItems         = ljc_get_cart_items();
//$aryHiddenCartSettings      = ljc_get_cart_settings_data();
$aryHiddenCartSettingsInfo  = ljc_wc_cart_settings_info();
//$aryHiddenCartDiamonds      = ljc_get_cart_diamonds_data();
$aryHiddenCartDiamondsInfo  = ljc_wc_cart_diamond_info();
//$objHiddenCartSettings      = $aryHiddenCartSettings[0];
//$objHiddenCartDiamonds      = $aryHiddenCartDiamonds[0];

//echo '<pre>';
//var_dump('$aryHiddenCartItems',$aryHiddenCartItems);
//echo "<br /><br />";
//var_dump('$aryHiddenCartSettingsInfo',$aryHiddenCartSettingsInfo);//[0]->setting_id; [0]->product_id; [0]->variable_id; [0]->parent_id;
//echo "<br /><br />";
//var_dump('$aryHiddenCartSettingsInfo[0]->setting_id',$aryHiddenCartSettingsInfo[0]->setting_id);
//echo '</pre>';
/* $aryHiddenCartItems
array(1) {
  ["settings"]=>
  array(2) {
    [99089912]=>
    array(2) {
      ["product_id"]=>
      int(9908)
      ["variation_id"]=>
      int(9912)
    }
    [99579963]=>
    array(2) {
      ["product_id"]=>
      int(9957)
      ["variation_id"]=>
      int(9963)
    }
  }
 */

/*####################
 * Determine what type of page we're on
 */###################
if (isset($objThis->taxonomy) && ($objThis->taxonomy == 'product_cat')) {           //This is a product category page
    if ($objThis->slug == $strSettingsCategorySlug) {           //This is the settings category page
        $bolIsSettingsCategoryPage  = true;
    }

    if ($objThis->slug == $strDiamondsCategorySlug) {           //This is the diamonds category page
        $bolIsDiamondsCategoryPage  = true;

        if ( ! $aryHiddenCartItems ) {                          //Step 1 is 'diamond' if no selection has been made
            $bolDiamondIsFirst  = true;
            $bolSettingIsFirst  = false;
            $strStep1           = $strDiamond;
            $strStep2           = $strSetting;
        }
    }
}

if (isset($objThis->post_type) && ($objThis->post_type == 'product')) {         //This is a product page
    global $product;

    $objTerms = get_the_terms($objThis->ID, 'product_cat');
    if ($objTerms) {
        $aryCategorySlugs = array();
        foreach ($objTerms as $objTerm) {
            $objCategory = get_term($objTerm->term_id, 'category');
            array_push($aryCategorySlugs, $objCategory->slug);
        }
    }
//var_dump('$aryCategorySlugs',$aryCategorySlugs);
//    $strCategories = $product->get_categories();//This generates a page-breaking error on ljcstaging (but not ljcdev)
//var_dump('$strCategories',$strCategories);//string(14) "$strCategories" string(214) "Design Your Ring, Solitaire"
//    $bolIsSettingProductPage = (strstr($strCategories, 'Settings')) ? true : false;
    $bolIsSettingProductPage = (in_array($strSettingsCategorySlug, $aryCategorySlugs)) ? true : false;
//    $bolIsDiamondProductPage = (strstr($strCategories, 'Diamonds')) ? true : false;
    $bolIsDiamondProductPage = (in_array($strDiamondsCategorySlug, $aryCategorySlugs)) ? true : false;
//    $bolIsCustomRingProductPage = (strstr($strCategories, 'Custom Rings')) ? true : false;
    $bolIsCustomRingProductPage = (in_array($strCustomRingsCategorySlug, $aryCategorySlugs)) ? true : false;

    if ($bolIsSettingProductPage) {                       //This is a setting product page
        //do stuff
    }

    if ($bolIsDiamondProductPage) {                       //This is a diamond product page
        if ( ! $aryHiddenCartItems ) {          //Step 1 is 'diamond' if no selection has been made
            $bolDiamondIsFirst  = true;
            $bolSettingIsFirst  = false;
            $strStep1           = $strDiamond;
            $strStep2           = $strSetting;
        }
    }
}

/*####################
 * Conditional vars, depending on the type and number of objects found
 */###################
if ($aryHiddenCartItems) {
    $bolIsBuilding          = true;

    $intItemsCount          = count($aryHiddenCartItems);
    $intSettingCount        = ( ! empty($aryHiddenCartItems['settings']) ) ? count($aryHiddenCartItems['settings']) : 0 ;
    $intDiamondCount        = ( ! empty($aryHiddenCartItems['diamonds']) ) ? count($aryHiddenCartItems['diamonds']) : 0 ;
    $intSelectionCount      = (
        ( ($intSettingCount > 0) ? 1 : 0 ) +
        ( ($intDiamondCount > 0) ? 1 : 0 )
    );

    if ( isset($aryHiddenCartItems['settings']) ) {
        reset($aryHiddenCartItems['settings']);
        $aryActiveSettingIds    = array_pop($aryHiddenCartItems['settings']);
        reset($aryHiddenCartSettingsInfo);
        $objActiveSetting       = array_pop($aryHiddenCartSettingsInfo);
//        $intSettingId           = (int) $objActiveSetting->setting_id;
        $intSettingId           = $aryActiveSettingIds['variation_id'];
//        $intSettingParentId     = (int) $objActiveSetting->product_id;    }
        $intSettingParentId     = $aryActiveSettingIds['product_id'];
    }

    if ( isset($aryHiddenCartItems['diamonds']) ) {
        reset($aryHiddenCartItems['diamonds']);
        $aryActiveDiamondIds    = array_pop($aryHiddenCartItems['diamonds']);
        reset($aryHiddenCartDiamondsInfo);
        $objActiveDiamond       = array_pop($aryHiddenCartDiamondsInfo);
//        $intDiamondId           = (int) $objActiveDiamond->diamond_id;
        $intDiamondId           = $aryActiveDiamondIds['product_id'];           //Should be 'variation_id' - todo: FIX
//        $intDiamondParentId     = (int) $objActiveDiamond->product_id;
        $intDiamondParentId     = $aryActiveDiamondIds['product_id'];
    }

    reset($aryHiddenCartItems);
    $strFirstKey                = key($aryHiddenCartItems);
    if ($strFirstKey == $strSettingsCategorySlug) {                     //Step 1 is 'setting'
        //do nothing - 'setting' assumed to be first
    }

    if ($strFirstKey == $strDiamondsCategorySlug) {                     //Step 1 is 'diamond'
        //make 'diamond' first
        $bolDiamondIsFirst      = true;
        $bolSettingIsFirst      = false;
        $strStep1               = $strDiamond;
        $strStep2               = $strSetting;
    }

//    if ($aryHiddenCartSettings) {
    if ($objActiveSetting) {
//        $strViewSettingLink     = $objHiddenCartSettings->url;
        $strViewSettingGuid     = $objActiveSetting->post->guid;
        $aryViewSettingUrl      = parse_url($strViewSettingGuid);
        $strViewSettingPath     = $aryViewSettingUrl['path'];
        $strViewSettingLink     = home_url($strViewSettingPath);
        $objHiddenCartSettings  = wc_get_product($intSettingId);
        $intHiddenSettingPrice  = number_format($objHiddenCartSettings->get_price(), 0);
        $strSettingPriceCss     =                                                       //Un-hide price
        $strSettingLinkCss      = '';                                                   //Un-hide links
        $bolSettingSelected     = true;

        $objPf      = new WC_Product_Factory();
        $objProduct = $objPf->get_product( $objActiveSetting->setting_id );

        if ( $objProduct ) {
            $bolSettingExists   = true;
        }
    }

//    if ($aryHiddenCartDiamonds) {
    if ($objActiveDiamond) {
//        $strViewDiamondLink     = $objHiddenCartDiamonds->url;
        $strViewDiamondGuid     = $objActiveDiamond->post->guid;
        $aryViewDiamondUrl      = parse_url($strViewDiamondGuid);
        $strViewDiamondPath     = $aryViewDiamondUrl['path'];
        $strViewDiamondLink     = home_url($strViewDiamondPath);
        $objHiddenCartDiamonds  = wc_get_product($intDiamondParentId);
        $intHiddenDiamondPrice  = number_format($objHiddenCartDiamonds->get_price(), 0);
        $strDiamondPriceCss     =                                                       //Un-hide price
        $strDiamondLinkCss      = '';                                                   //Un-hide links
        $bolDiamondSelected     = true;

        $objPf      = new WC_Product_Factory();
        $objProduct = $objPf->get_product( $objActiveDiamond->id );

        if ( $objProduct ) {
            $bolDiamondExists   = true;
        }
    }

    if ($bolSettingSelected && $bolDiamondSelected) {
        $strStep3PriceCss       = '';
        $intStep3Price          = number_format(
	                                    (int) $objHiddenCartSettings->get_price() +
	                                    (int) $objHiddenCartDiamonds->get_price(),
                                        0
                                    );
    }
}
//global $woocommerce;
//echo '<pre>';
//var_dump('$woocommerce', $woocommerce);
//echo '</pre>';
//var_dump('$intSettingId',$intSettingId);
//echo '<br /><br />';
//var_dump('$intSettingParentId',$intSettingParentId);
//echo '<br /><br />';
//var_dump('$intDiamondId',$intDiamondId);
//echo '<br /><br />';
//var_dump('$intDiamondParentId',$intDiamondParentId);
//echo '<br /><br />';
//var_dump('$objActiveSetting',$objActiveSetting);
//echo '<br /><br />';
//var_dump('$aryActiveSettingIds',$aryActiveSettingIds);
//echo '<br /><br />';
//var_dump('$aryActiveDiamondIds',$aryActiveDiamondIds);
//echo '<br /><br />';
//var_dump('$objHiddenCartDiamonds',$objHiddenCartDiamonds);
//echo '<br /><br />';
//var_dump('$bolDiamondSelected',$bolDiamondSelected);
//var_dump('$bolSettingSelected',$bolSettingSelected);
/*####################
 * Determine if we show the bread crumbs, what's on them, and which one is active
 */###################
if ($intSelectionCount > 0) {
    $strSelectionCountText      = " ($intSelectionCount)";
}
//added by allan to show breadcrumb via shortcode
global $show_breadcrumb;

$bolIsASettingStep              = ( $bolIsSettingsCategoryPage || $bolIsSettingProductPage ) ? true : false;
$bolIsADiamondStep              = ( $bolIsDiamondsCategoryPage || $bolIsDiamondProductPage ) ? true : false;
$bolShowCrumbs                  = (
    $bolIsASettingStep ||
    $bolIsADiamondStep ||
    $show_breadcrumb ||
    $bolIsCompleteUri ||
    $bolIsCustomRingProductPage//  ||
//                                    $bolIsBuilding
) ? true : false;

$strSettingsStepWrapperClass    = ($bolIsASettingStep) ? $strActiveCrumbClass : '';
$strDiamondsStepWrapperClass    = ($bolIsADiamondStep) ? $strActiveCrumbClass : '';
//$strCompleteStepWrapperClass    = ( ($bolSettingSelected && $bolDiamondSelected) || $bolIsCompleteUri ) ? $strActiveCrumbClass : '';
$strCompleteStepWrapperClass    = ($bolIsCompleteUri) ? $strActiveCrumbClass : '';

if ($strStep1 == $strSetting) {
    if ($bolSettingSelected) {
        $strStep1Text = $strShortSettingText;
    } else {
        $strStep1Text = $strLongSettingText;
    }
    $strStep1PriceCss   = $strSettingPriceCss;
    $intStep1Price      = $intHiddenSettingPrice;
    $strStep1LinkCss    = $strSettingLinkCss;
    $strStep1ViewLink   = $strViewSettingLink;
    $strStep1ChangeLink = $strChangeSettingLink;
    $bolStep1Selected   = $bolSettingSelected;
} else {
    if ($bolSettingSelected) {
        $strStep2Text = $strShortSettingText;
    } else {
        $strStep2Text = $strLongSettingText;
    }
    $strStep2PriceCss   = $strSettingPriceCss;
    $intStep2Price      = $intHiddenSettingPrice;
    $strStep2LinkCss    = $strSettingLinkCss;
    $strStep2ViewLink   = $strViewSettingLink;
    $strStep2ChangeLink = $strChangeSettingLink;
    $bolStep2Selected   = $bolSettingSelected;
}

if ($strStep1 == $strDiamond) {
    if ($bolDiamondSelected) {
        $strStep1Text = $strShortDiamondText;
    } else {
        $strStep1Text = $strLongDiamondText;
    }
    $strStep1PriceCss   = $strDiamondPriceCss;
    $intStep1Price      = $intHiddenDiamondPrice;
    $strStep1LinkCss    = $strDiamondLinkCss;
    $strStep1ViewLink   = $strViewDiamondLink;
    $strStep1ChangeLink = $strChangeDiamondLink;
    $bolStep1Selected   = $bolDiamondSelected;
} else {
    if ($bolDiamondSelected) {
        $strStep2Text = $strShortDiamondText;
    } else {
        $strStep2Text = $strLongDiamondText;
    }
    $strStep2PriceCss   = $strDiamondPriceCss;
    $intStep2Price      = $intHiddenDiamondPrice;
    $strStep2LinkCss    = $strDiamondLinkCss;
    $strStep2ViewLink   = $strViewDiamondLink;
    $strStep2ChangeLink = $strChangeDiamondLink;
    $bolStep2Selected   = $bolDiamondSelected;
}
//var_dump('$strStep2Text', $strStep2Text);//Choose Your Diamond
if ($bolIsASettingStep) {
    if ($bolSettingIsFirst) {
//        if ($bolSettingSelected) {
//            $strStep1Text = $strShortSettingText;
//        } else {
//            $strStep1Text = $strLongSettingText;
//        }
        $strStep1WrapperClass   = $strSettingsStepWrapperClass;
    } else {
//        if ($bolSettingSelected) {
//            $strStep2Text = $strShortSettingText;
//        } else {
//            $strStep2Text = $strLongSettingText;
//        }
        $strStep2WrapperClass   = $strSettingsStepWrapperClass;
    }
}

if ($bolIsADiamondStep) {
    if ($bolDiamondIsFirst) {
//        if ($bolDiamondSelected) {
//            $strStep1Text = $strShortDiamondText;
//        } else {
//            $strStep1Text = $strLongDiamondText;
//        }
        $strStep1WrapperClass   = $strDiamondsStepWrapperClass;
    } else {
//        if ($bolDiamondSelected) {
//            $strStep2Text = $strShortDiamondText;
//        } else {
//            $strStep2Text = $strLongDiamondText;
//        }
        $strStep2WrapperClass   = $strDiamondsStepWrapperClass;
    }
}

//if ( ($bolSettingSelected && $bolDiamondSelected) || $bolIsCustomRingProductPage ) {
if ( $bolIsCompleteUri || $bolIsCustomRingProductPage ) {
    $strStep1WrapperClass       = '';
    $strStep2WrapperClass       = '';
    $strStep3WrapperClass       = $strCompleteStepWrapperClass;
}
//var_dump('$strStep2Text', $strStep2Text);//Diamond

if ($bolShowCrumbs) {
?>
    <div class="e3ve-breadcrumbs">
        <div class="e3ve-breadcrumbs-wrapper">
            <div class="e3ve-breadcrumbs-step-0">Design Your Ring</div>
            <div class="e3ve-arrow-forward-grey"></div>
            <div id="step_1" class="e3ve-breadcrumbs-step-wrapper<?php echo $strStep1WrapperClass ?>">
                <div class="e3ve-arrow-forward-white"></div>
                <div class="e3ve-breadcrumbs-step-1">
                    <div class="e3ve-big-number">
                        <div data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="78"
                             data-lineheight="89">1</div>
                    </div>
                    <div class="e3ve-step-content" name="e3ve-step-content">
                        <p class="p-1"><?php echo $strStep1Text ?></p>
                        <?php
                        if ($bolStep1Selected) {
                            ?>

                            <p class="p-2"<?php echo $strStep1PriceCss ?>>$<?php echo $intStep1Price ?></p>

                            <p class="p-3"<?php echo $strStep1LinkCss ?>><a
                                    href="<?php echo $strStep1ViewLink ?>">View</a> | <a
                                    href="<?php echo $strStep1ChangeLink ?>">Change</a></p>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="e3ve-arrow-forward-grey"></div>
            </div>
            <div id="step_2" class="e3ve-breadcrumbs-step-wrapper<?php echo $strStep2WrapperClass ?>">
                <div class="e3ve-arrow-forward-white"></div>
                <div class="e3ve-breadcrumbs-step-1">
                    <div class="e3ve-big-number">
                        <div data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="78"
                             data-lineheight="89">2</div>
                    </div>
                    <div class="e3ve-step-content" name="e3ve-step-content">
                        <p class="p-1"><?php echo $strStep2Text ?></p>
                        <?php
                        if ($bolStep2Selected) {
                            ?>

                            <p class="p-2"<?php echo $strStep2PriceCss ?>>$<?php echo $intStep2Price ?></p>

                            <p class="p-3"<?php echo $strStep2LinkCss ?>><a
                                    href="<?php echo $strStep2ViewLink ?>">View</a> | <a
                                    href="<?php echo $strStep2ChangeLink ?>">Change</a></p>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="e3ve-arrow-forward-grey"></div>
            </div>
            <div id="step_3" class="e3ve-breadcrumbs-step-wrapper<?php echo $strStep3WrapperClass ?>">
                <div class="e3ve-arrow-forward-white"></div>
                <div class="e3ve-breadcrumbs-step-1">
                    <div class="e3ve-big-number">
                        <div data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="78"
                             data-lineheight="89">3</div>
                    </div>
                    <div class="e3ve-step-content" name="e3ve-step-content">
                        <p class="p-1">Complete<br />Your Ring</p>
                        <?php
                        if ($bolStep1Selected && $bolStep2Selected) {
                            ?>
                            <p class="p-2"<?php echo $strStep3PriceCss ?>>$<?php echo $intStep3Price ?></p>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <!--New Selection Dropdown Start-->
            <div class="e3ve-selection-dropdown">
                <div class="e3ve-selection-dropdown-inner" style="height:auto; margin:25px 15px">
                    <div id="your_selections_dropdown" class="default-selection e3ve-selection-dropdown-select" style="position:relative">Your Selections <?php echo $strSelectionCountText ?></div>
                </div>
                <div id="your_selections_dropdown_content" class="e3ve-inner-selection" style="z-index: 9; position: absolute; background: rgb(225, 225, 225) none repeat scroll 0px 0px; padding: 5px; width: 13%;">
                    <?php
                    $intI = 1;
                    if ($aryHiddenCartItems) {
                        foreach ($aryHiddenCartItems as $key => $value) {
                            if ( $key == 'settings' && ( ! empty($objActiveSetting) ) && ($bolSettingExists) ) {
                                ?>
                                <div class="selection<?php echo $intI ?>">
                                    <div style="display:inline-block;" class="e3ve-selection-drpdwn-container">
                                        <div style="float:left; width:28%; padding:1%;" class="e3ve-selection-drpdwn1">
                                            <?php echo $objActiveSetting->get_image();
                                                echo "<!--\r\n";
                                                var_dump('$objActiveSetting', $objActiveSetting);
                                                echo "-->\r\n";
                                            ?>
                                        </div>
                                        <div class="e3ve-selection-drpdwn2" style="float:left; width:67%; padding:1%; margin-left:5%">
                                            <div style="line-height: normal; margin: 0px auto;" class=""><?php echo $objActiveSetting->post->post_title; ?></div>
                                            <div style="line-height: normal; margin: 3px auto auto 3px; font-weight: 800;" class=""><?php echo '$' . $intHiddenSettingPrice . '.00'; ?></div>
                                            <div class="e3ve-selection-drpdwn-links" style="font-weight:100; text-transform:uppercase; font-size:10px">
                                                <a href="<?php echo $strViewSettingLink; ?>" style="cursor: pointer; text-decoration: underline">View</a> | <a href="<?php echo $strChangeSettingLink; ?>" style="cursor: pointer; text-decoration: underline">Change</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if ( $key == 'diamonds' && ( ! empty($objActiveDiamond) ) && ($bolDiamondExists) ) {
                                ?>
                                <div class="selection<?php echo $intI ?>">
                                    <div style="display:inline-block;" class="e3ve-selection-drpdwn-container">
                                        <div style="float:left; width:28%; padding:1%;" class="e3ve-selection-drpdwn1">
                                            <?php echo $objActiveDiamond->get_image();
                                                echo "<!--\r\n";
                                                var_dump('$objActiveDiamond', $objActiveDiamond);
                                                echo "-->\r\n";
                                            ?>
                                        </div>
                                        <div class="e3ve-selection-drpdwn2" style="float:left; width:67%; padding:1%; margin-left:5%">
                                            <div style="line-height: normal; margin: 0px auto;" class=""><?php echo $objActiveDiamond->post->post_title; ?></div>
                                            <div style="line-height: normal; margin: 3px auto auto 3px; font-weight: 800;" class=""><?php echo '$' . $intHiddenDiamondPrice . '.00'; ?></div>
                                            <div class="e3ve-selection-drpdwn-links" style="font-weight:100; text-transform:uppercase; font-size:10px">
                                                <a href="<?php echo $strViewDiamondLink; ?>" style="text-decoration:underline">View</a> | <a href="<?php echo $strChangeDiamondLink; ?>" style="text-decoration:underline">Change</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            $intI++;
                        }
                    }
                    if ($intSelectionCount > 0) {
                        ?>
                        <div id="reset_selections" class="default-selection" style="cursor: pointer; text-align: center; text-transform: uppercase; padding-top: 5px; border-top: 1px solid rgb(190, 190, 190);">Reset Selections</div>
                    <?php
                    }
                    ?>
                        <div id="manage_selections" class="default-selection" style="cursor: pointer; text-align: center; text-transform: uppercase; padding-top: 5px; border-top: 1px solid rgb(190, 190, 190);" data-toggle="modal" data-target="#myModal">Manage Selections</div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 83%; height: 80%;">

                <!-- Modal content-->
                <div class="modal-content" style="width: 100%; height: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Manage Your Selections</h4>
                    </div>
                    <div class="modal-body" style="height: 90%;">
                        <iframe style="border: 0px; " src="<?php echo $strFullCompleteUrl ?>" width="100%" height="100%"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('#step_1,#step_2,#step_3,.e3ve-selection-dropdown').css('cursor', 'pointer');
        });

        jQuery('#step_1').on('click', function() {
            location.href='<?php echo ($strStep1ViewLink == '#') ? $strStep1ChangeLink : $strStep1ViewLink; ?>';
        });

        jQuery('#step_2').on('click', function() {
            location.href='<?php echo ($strStep2ViewLink == '#') ? $strStep2ChangeLink : $strStep2ViewLink; ?>';
        });

        //    if ('<?php //echo $intSelectionCount ?>//' == '2') {
        jQuery('#step_3').on('click', function() {
            location.href = '<?php echo $strCompleteUrl ?>';
        });
        //    }

//        jQuery('#manage_selections').on('click', function() {
//            //launch modal
//        });

        //    jQuery('select[name="carlist"]').change(function() {
        //        if (jQuery(this).val() == 'reset') {
        //            alert ('feature not yet enabled');
        //        }
        //    });

        jQuery('#myModal').on('hidden.bs.modal', function () {
            location.reload();
        });
    </script>
<?php
}
/*####################
 * Custom functions
 */###################

/**
 * Retrieve the ID of a product category from its name.
 *
 * @since 1.0.0
 *
 * @param string $cat_name Product Category name.
 * @return int 0, if failure and ID of category on success.
 */
function get_product_cat_ID( $cat_name ) {
    $cat = get_term_by( 'name', $cat_name, 'product_cat' );
    if ( $cat )
        return $cat->term_id;
    return 0;
}

/**
 * Retrieve category object by category slug.
 *
 * @since 2.3.0
 *
 * @param string $slug The category slug.
 * @return object Category data object
 */
function get_product_category_by_slug( $slug ) {
    $category = get_term_by( 'slug', $slug, 'product_cat' );
    if ( $category )
        _make_cat_compat( $category );

    return $category;
}
?>

(function( $ ) {
	'use strict';
	/**
	 * possible label
	 * warning
	 * danger
	 * default
	 * primary
	 * info
	 * success
	 * */
	function msg_content(_msg, msg_div, _label){
		var _msg_div = msg_div;
		if( !_label ){
			_label = 'info';
		}
		_msg_div.html('');
		_msg_div.html('<h2><span class="label label-'+_label+'">'+_msg+'</span></h2>');
	}
		
	var get_add_to_cart_trigger = function(){
		return {
			init:function(){
				$('body').on('added_to_cart',function() {
					//console.log('a product was added!');
					var $fragment_refresh = {
						url: wc_cart_fragments_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
						type: 'POST',
						success: function( data ) {
							if ( data && data.fragments ) {
					 
								$.each( data.fragments, function( key, value ) {
									$( key ).replaceWith( value );
								});
					 
								if ( $supports_html5_storage ) {
									sessionStorage.setItem( wc_cart_fragments_params.fragment_name, JSON.stringify( data.fragments ) );
									set_cart_hash( data.cart_hash );
					 
									if ( data.cart_hash ) {
										set_cart_creation_timestamp();
									}
								}
					 
								$( document.body ).trigger( 'wc_fragments_refreshed' );
							}
						}
					};
					$.ajax( $fragment_refresh );
				});
			}
		};
	}();

	var get_add_to_woo_cart_trigger = function(){
		return {
			init:function(){
				//ljc_add_to_cart_finish
				$('body').on('ljc_add_to_cart_finish',function(e, data) {
					console.log(data);
				});
			}
		};
	}();
	
	var refresh_saved_container_trigger = function(){
		return{
			init:function(){
				$('body').on('refresh_saved_container',function(event, data) {
					console.log('refresh saved container');
					if( data.status == 1 ){
						//reload the save selection container
						var _save_selection_container = $('.save-selection-content');
						_save_selection_container.html('<h2>'+LJC_JS.ajax_temp_cart_getting_saved_selections_container+'</h2>');
						var data = [
							{name: 'action', value: 'get_save_selection'}
						];
						$.ajax({
							type: "POST",
							url: LJC_JS.ajaxurl,
							data: data,
							dataType: "html"
						}).done(function( data ) {
							_save_selection_container.html(data);
						});
					}
				});
			}
		};
	}();
	
	var ajax_remove_saved_selection = function(){
		return{
			init:function(){
				var _setting_id = 0;
				var _diamond_id = 0;
				var msg_div;
				var loop_cart_div;
				var idx = 0;
				$(document).on('click','.remove-to-cart-selection',function(e){
					if( $(this).data('setting-id') !== undefined ){ //add to cart in loop
						_setting_id = $(this).data('setting-id');
						//loop cart msg
						idx = $(this).data('idx');
						loop_cart_div = $('.loop-cart-ajax-msg-' + idx);
						loop_cart_div.show();
						loop_cart_div.html('');
						msg_content(LJC_JS.ajax_temp_cart_deleting_item, loop_cart_div);
					}
					if( $(this).data('diamond-id') !== undefined ){ //add to cart in loop
						_diamond_id = $(this).data('diamond-id');
					}
					var data = [
						{name: 'action', value: 'remove_save_selection'},
						{name: 'setting_id', value: _setting_id},
						{name: 'diamond_id', value: _diamond_id}
					];
					$.ajax({
						type: "POST",
						url: LJC_JS.ajaxurl,
						data: data,
						dataType: "json"
					}).done(function( data ) {
						//_save_selection_container.html(data);
						//console.log(data);
						if(data.status == 1 ){
							$( document.body ).trigger( 'refresh_saved_container', [data]);
						}else{
							loop_cart_div = $('.loop-cart-ajax-msg-' + idx);
							msg_content(data.msg, loop_cart_div, 'warning');
							loop_cart_div.delay(5000).fadeOut(500);
						}
					});	
				});
			}
		};
	}();
	
	var ajax_save_selection = function(){
		function get_setting_dropdown(){
			var _id = 0;
			var _setting_dropdown = $('#dropdown-settings');
			_id = _setting_dropdown.val();
			return _id;
		}
		function get_diamond_dropdown(){
			var _id = 0;
			var _diamond_dropdown = $('#dropdown-diamond');
			_id = _diamond_dropdown.val();
			return _id;
		}
		return {
			init:function(){
				var msg_div = $('.add-selection-msg');
				var _add_selection_button = $('.add-selection');
				var _add_selection_button_div = $('.save-add-button');
				$(document).on('click','.add-selection', function(e){
					msg_div.show();
					_add_selection_button_div.hide();
					msg_content(LJC_JS.ajax_save_selection_msg_success, msg_div);
					var _setting_id = get_setting_dropdown();
					var _diamond_id = get_diamond_dropdown();
					var data = [
						{name: 'action', value: 'save_selection'},
						{name: 'security', value: LJC_JS.security},
						{name: 'setting_id', value: _setting_id},
						{name: 'diamond_id', value: _diamond_id}
					];
					$.ajax({
						type: "POST",
						url: LJC_JS.ajaxurl,
						data: data,
						dataType: "json"
					}).done(function( data ) {
						var _label = 'info';
						if( data.status == 1 ){
							_label = 'success';
						}else if(data.status == 2){
							_label = 'warning';
						}
						msg_content(data.msg, msg_div, _label);
						msg_div.delay(5000).fadeOut(500);
						_add_selection_button_div.show();
						$( document.body ).trigger( 'refresh_saved_container', [data]);
						$( document.body ).trigger( 'added_saved_selection', [data]);
					});
				});//click
			}
		};
	}();
	
	var ajax_add_to_cart = function(){
		return {
			init: function(){
				var _setting_id = 0;
				var _diamond_id = 0;
				var msg_div;
				var is_dropdown_choose = 0;
				var loop_cart_div;
				var idx = 0;
				var variation_id = 0;
				var setting_ring_size = 0;
				$(document).on('click', '.add-to-cart-selection',function(){
					if( $(this).data('setting-id') !== undefined ){ //add to cart in loop
						_setting_id = $(this).data('setting-id');
						//loop cart msg
						idx = $(this).data('idx');
						loop_cart_div = $('.loop-cart-ajax-msg-' + idx);
						loop_cart_div.show();
						loop_cart_div.html('');
						msg_content(LJC_JS.ajax_temp_cart_add_to_wc_cart, loop_cart_div);
					}else{//might be dropdown
						is_dropdown_choose = 1;
						msg_div = $('.add-selection-msg');
						msg_div.show();
						msg_div.html('');
						msg_content(LJC_JS.ajax_temp_cart_add_to_wc_cart, msg_div);
						_setting_id = $('#dropdown-settings').val();
					}
					if( $(this).data('diamond-id') !== undefined ){ //add to cart in loop
						_diamond_id = $(this).data('diamond-id');
					}else{//might be dropdown
						_diamond_id = $('#dropdown-diamond').val();
					}
					if( $(this).data('setting-variation-id') !== undefined ){
						variation_id = $(this).data('setting-variation-id');
					}else{
						//dropdown
						variation_id = $('#dropdown-settings').find(':selected').data('setting-variation-id');
					}
					if( $(this).data('setting-ring-size') !== undefined ){
						setting_ring_size = $(this).data('setting-ring-size');
					}else{
						//dropdown
						setting_ring_size = $('#dropdown-settings').find(':selected').data('setting-ring-size');
					}
					/*console.log(_setting_id);
					console.log(variation_id);
					console.log(setting_ring_size);
					console.log(_diamond_id);*/
					var data = [
						{name: 'action', value: 'ljc_add_to_cart'},
						{name: 'security', value: LJC_JS.security},
						{name: 'setting_id', value: _setting_id},
						{name: 'setting_variation_id', value: variation_id},
						{name: 'setting_ring_size', value: setting_ring_size},
						{name: 'diamond_id', value: _diamond_id}
					];
					//console.log(data);
					$.ajax({
						type: "POST",
						url: LJC_JS.ajaxurl,
						data: data,
						dataType: "json",
						cache: false
					}).done(function( data ) {
						//console.log(data);
						//console.log(is_dropdown_choose);
						if( data.status == 1 ){
							if( is_dropdown_choose == 1 ){
								msg_div = $('.add-selection-msg');
								msg_content(LJC_JS.ajax_temp_cart_added_to_wc_cart, msg_div, 'success');
								msg_div.delay(5000).fadeOut(500);
							}else{
								loop_cart_div = $('.loop-cart-ajax-msg-' + idx);
								msg_content(LJC_JS.ajax_temp_cart_added_to_wc_cart, loop_cart_div, 'success');
								loop_cart_div.delay(5000).fadeOut(500);
							}
							window.location.href = data.woo_cart_url;
						}else{
							if( is_dropdown_choose == 1 ){
								msg_div = $('.add-selection-msg');
								msg_content(LJC_JS.ajax_temp_cart_added_to_wc_cart_fail, msg_div, 'danger');
								msg_div.delay(5000).fadeOut(500);
							}else{
								loop_cart_div = $('.loop-cart-ajax-msg-' + idx);
								msg_content(LJC_JS.ajax_temp_cart_added_to_wc_cart_fail, loop_cart_div, 'danger');
								loop_cart_div.delay(5000).fadeOut(500);	
							}
						}
						$( document.body ).trigger( 'ljc_add_to_cart_finish', [data] );
					});
				});
			}
		};
	}();
	/**
	 * remove single setting
	 * */
	var remove_setting = function(){
		return {
			init:function(){
				var current_setting_id = $('#dropdown-settings');
				var current_setting_sel = $("#dropdown-settings option:selected");
				var remove_button = $('.remove-setting-selection');
				var msg_div = $('.dropdown-selection-msg');
				$(document).on('click', '.remove-setting-selection',function(){
					msg_div.show();
					msg_div.html('');
					msg_content(LJC_JS.ajax_remove_this_setting_item_progress, msg_div, 'info');
					var current_setting_sel = $("#dropdown-settings option:selected");
					var data = [
						{name: 'action', value: 'ljc_remove_setting'},
						{name: 'security', value: LJC_JS.security},
						{name: 'setting_id', value: current_setting_id.val()}
					];
					$.ajax({
						type: "POST",
						url: LJC_JS.ajaxurl,
						data: data,
						dataType: "json"
					}).done(function( data ) {
						if( data.status == 1 ){
							current_setting_sel.remove();
							msg_content(LJC_JS.ajax_remove_this_setting_item_success, msg_div, 'success');
							msg_div.delay(5000).fadeOut(500);
							$( document.body ).trigger( 'ajax_ljc_remove_setting' );
						}else{
							msg_content(LJC_JS.ajax_remove_this_setting_item_fail, msg_div, 'warning');
							msg_div.delay(5000).fadeOut(500);
							$( document.body ).trigger( 'ajax_ljc_remove_setting_fail' );
						}
					});
				});
			}
		};
	}();
	/**
	 * remove single diamond
	 * */
	var remove_diamond = function(){
		return {
			init:function(){
				var current_diamond_id = $('#dropdown-diamond');
				var current_diamond_sel = $("#dropdown-diamond option:selected");
				var remove_button = $('.remove-diamond-selection');
				var msg_div = $('.dropdown-selection-msg');
				$(document).on('click', '.remove-diamond-selection',function(){
					msg_div.show();
					msg_div.html('');
					msg_content(LJC_JS.ajax_remove_this_diamond_item_progress, msg_div, 'info');
					var current_diamond_sel = $("#dropdown-diamond option:selected");
					//
					var data = [
						{name: 'action', value: 'ljc_remove_diamond'},
						{name: 'security', value: LJC_JS.security},
						{name: 'diamond_id', value: current_diamond_id.val()}
					];
					$.ajax({
						type: "POST",
						url: LJC_JS.ajaxurl,
						data: data,
						dataType: "json"
					}).done(function( data ) {
						if( data.status == 1 ){
							current_diamond_sel.remove();
							msg_content(LJC_JS.ajax_remove_this_diamond_item_success, msg_div, 'success');
							msg_div.delay(5000).fadeOut(500);
							$( document.body ).trigger( 'ajax_ljc_diamond_setting' );
						}else{
							msg_content(LJC_JS.ajax_remove_this_diamond_item_fail, msg_div, 'warning');
							msg_div.delay(5000).fadeOut(500);
						}
						$( document.body ).trigger( 'ajax_ljc_diamond_setting_fail' );
					});
				});
			}
		};
	}();
	
	var reset_breadcrumb_selections = function(){
		return {
			init:function(){
				//$(document).on('change', '.e3ve-selection-dropdown-select', function(){
				$(document).on('click', '#reset_selections', function(){
					//var _dropdown_selection = $(this).val();
					//if( _dropdown_selection == 'reset' ){
						var data = [
							{name: 'action', value: 'ljc_reset_selection'},
							{name: 'security', value: LJC_JS.security},
						];
						$.ajax({
							type: "POST",
							url: LJC_JS.ajaxurl,
							data: data,
							dataType: "json"
						}).done(function( data ) {
							if( data.status == 1 ){
								//msg_content(LJC_JS.ajax_remove_this_diamond_item_success, msg_div, 'success');
								//msg_div.delay(5000).fadeOut(500);
								location.reload();
							}else{
								//msg_content(LJC_JS.ajax_remove_this_diamond_item_fail, msg_div, 'warning');
								//msg_div.delay(5000).fadeOut(500);
								location.reload();
							}
						});
					//}
				});
			}
		};
	}();
	
	var ajax_save_comparison_table = function(){
		return{
			init:function(){
				$('body').on('add_comparison_table',function(e, data) {
					var data = [
						{name: 'action', value: 'ljc_comparison_store_db'},
						{name: 'security', value: LJC_JS.security},
						{name: 'id', value: data.id},
					];
					$.ajax({
						type: "POST",
						url: LJC_JS.ajaxurl,
						data: data,
						dataType: "json"
					}).done(function( data ) {
						if( data.status == 1 ){
							//msg_content(LJC_JS.ajax_remove_this_diamond_item_success, msg_div, 'success');
							//msg_div.delay(5000).fadeOut(500);
							//location.reload();
						}else{
							//msg_content(LJC_JS.ajax_remove_this_diamond_item_fail, msg_div, 'warning');
							//msg_div.delay(5000).fadeOut(500);
							//location.reload();
						}
					});
				});
			}
		};
	}();
	
	var ajax_del_comparison_table = function(){
		return{
			init:function(){
				$('body').on('remove_comparison_table',function(e, data) {
					var data = [
						{name: 'action', value: 'ljc_del_comparison_db'},
						{name: 'security', value: LJC_JS.security},
						{name: 'id', value: data.id},
					];
					$.ajax({
						type: "POST",
						url: LJC_JS.ajaxurl,
						data: data,
						dataType: "json"
					}).done(function( data ) {
						if( data.status == 1 ){
							//msg_content(LJC_JS.ajax_remove_this_diamond_item_success, msg_div, 'success');
							//msg_div.delay(5000).fadeOut(500);
							//location.reload();
						}else{
							//msg_content(LJC_JS.ajax_remove_this_diamond_item_fail, msg_div, 'warning');
							//msg_div.delay(5000).fadeOut(500);
							//location.reload();
						}
					});
				});
			}
		};
	}();
	
	var ion = function(){
		return{
			init:function(){
				$('.pf_rng_pa_center-stone-minimum-size').ionRangeSlider({
					from:0.8,
					onStart: function (data) {
						console.log(data);
					},
					onChange: function (data) {
						console.log(data);
					},
					onFinish: function (data) {
						console.log(data);
					},
					onUpdate: function (data) {
						console.log(data);
					}
				});
			}
		};
	}();
	
	$(window).load(function(){
		get_add_to_cart_trigger.init();
		//save selection
		ajax_save_selection.init();
		//selection add to cart
		ajax_add_to_cart.init();
		//reload save selection container
		refresh_saved_container_trigger.init();
		//remove saved selection
		ajax_remove_saved_selection.init();
		//remove current setting item
		remove_setting.init();
		//remove current diamond item
		remove_diamond.init();
		reset_breadcrumb_selections.init();
		//trigger to add woo cart
		get_add_to_woo_cart_trigger.init();
		//ion.init();
		ajax_save_comparison_table.init();
		ajax_del_comparison_table.init();
	});
})( jQuery );

var options_share_datatable = {
	"ordering": true,
	"paging": false,
	"info": false,
	"scrollY":"50vh",
	"scrollCollapse": true
};
var diamond_table = jQuery('#loopDatatables').DataTable(options_share_datatable);
var compare_table = jQuery('#compareDataTable').DataTable(options_share_datatable);
var recent_view_table = jQuery('#loopRecentViewDatatables').DataTable(options_share_datatable);

function ljc_count_compare_table(){
	var count = jQuery('#compareDataTable > tbody > tr').length;
	jQuery('.compare-count').html("");
	jQuery('.compare-count').html("(" + count + ")");
}

function right_info(show){
	if( !show ){
		show = false;
	}
	if( !show ){
		jQuery('.hover_info').hide();
	}else{
		jQuery('.hover_info').show();
	}
}

function right_noinfo(show){
	if( !show ){
		show = false;
	}
	if( !show ){
		jQuery('.no_info').hide();
		jQuery('.info_stock_url').show();
	}else{
		jQuery('.no_info').show();
		jQuery('.info_stock_url').hide();
	}
}

function get_img($post_id){
	if (typeof eval('rapnet_dl_image_' + $post_id) !== 'undefined') {
		return eval('rapnet_dl_image_' + $post_id);
	}
}

function init_hover($post_id, hover_data){
	var diamond_url = '';
	if ( eval('rapnet_dl_url_' + $post_id) !== 'undefined') {
		// the variable is defined
		var diamond_url = eval('rapnet_dl_url_' + $post_id);
	}
	
	var rapnet_dl = '';
	if ( eval('rapnet_dl_' + $post_id) !== 'undefined') {
		// the variable is defined
		var rapnet_dl = eval('rapnet_dl_' + $post_id);
	}
	jQuery('.info_stock_number').html('');
	jQuery('.info_stock_number').html(rapnet_dl.stock_number);
	jQuery('.info_stock_shape').html('');
	jQuery('.info_stock_shape').html(rapnet_dl.shape);
	jQuery('.info_stock_carat_weight').html('');
	jQuery('.info_stock_carat_weight').html(rapnet_dl.weight);
	jQuery('.info_stock_color').html('');
	jQuery('.info_stock_color').html(rapnet_dl.color);
	jQuery('.info_stock_report').html('');
	jQuery('.info_stock_report').html(hover_data[6]);
	jQuery('.info_stock_polish').html('');
	jQuery('.info_stock_polish').html(rapnet_dl.polish);
	jQuery('.info_stock_depth').html('');
	jQuery('.info_stock_depth').html(rapnet_dl.depth);
	jQuery('.info_stock_girdle').html('');
	jQuery('.info_stock_girdle').html(rapnet_dl.girdle);
	jQuery('.info_stock_lw_ratio').html('');
	//jQuery('.info_stock_lw_ratio').html(rapnet_dl.girdle);
	jQuery('.info_stock_price').html('');
	jQuery('.info_stock_price').html(hover_data[7]);
	jQuery('.info_stock_cut').html('');
	jQuery('.info_stock_cut').html(rapnet_dl.cut);
	jQuery('.info_stock_clarity').html('');
	jQuery('.info_stock_clarity').html(rapnet_dl.clarity);
	jQuery('.info_stock_measurements').html('');
	jQuery('.info_stock_measurements').html(rapnet_dl.measurements);
	jQuery('.info_stock_symmetry').html('');
	jQuery('.info_stock_symmetry').html(rapnet_dl.symmetry);
	jQuery('.info_stock_table').html('');
	jQuery('.info_stock_table').html(rapnet_dl.table);
	jQuery('.info_stock_cutlet').html('');
	jQuery('.info_stock_cutlet').html(rapnet_dl.culet);
	jQuery('.info_stock_fluorescence').html('');
	jQuery('.info_stock_fluorescence').html(rapnet_dl.fluorescence_color);
	jQuery('.info_stock_url').attr("href", diamond_url);
}

var diamondTable = function(dtOption, diamond_table, compare_table, init_hover){
	var hover_data;
	var table = diamond_table;
	jQuery(document).on('hover','#loopDatatables tbody tr',function(e){
		var data = table.row( this ).data();
		var post_id = jQuery(this).data('post-id');
		right_noinfo();
		right_info(true);

		hover_data = data;
		init_hover(post_id, hover_data);
	});

	jQuery(document).on('click','#loopDatatables tbody .loopDT',function(e){
		var checkbox_val = jQuery(this).val();
		var data = {
			id:checkbox_val
		}
		if(jQuery(this).is(':checked')){
			var data_row = [
				hover_data[0],
				hover_data[1],
				hover_data[2],
				hover_data[3],
				hover_data[4],
				hover_data[5],
				hover_data[6],
				hover_data[7],
				hover_data[8]
			];
			
			var rowNode = compare_table
			.row.add(data_row)
			.draw()
			.nodes()
			.to$()
			.addClass( 'tr-post-id-' + checkbox_val )
			.find('.loopDT')
			.prop('checked', true);
			
			ljc_count_compare_table();
			jQuery( document.body ).trigger( 'add_comparison_table', [data]);
		}else{
			compare_table.row('.tr-post-id-' + checkbox_val).remove().draw( false );
			ljc_count_compare_table();
			jQuery( document.body ).trigger( 'remove_comparison_table', [data]);
		}
	});  
};
var reviewDataTable = function(dtOption, recent_view_table, compare_table, init_hover){
	var hover_data;
	var table = recent_view_table;
	jQuery(document).on('hover','#loopRecentViewDatatables tbody tr',function(e){
		var data = table.row( this ).data();
		var post_id = jQuery(this).data('post-id');
		hover_data = data;
		console.log(post_id);
		console.log(data);
		init_hover(post_id, hover_data);
	});
	jQuery(document).on('click','#loopRecentViewDatatables tbody .loopDT',function(e){
		var checkbox_val = jQuery(this).val();
		var data = {
			id:checkbox_val
		}
		if(jQuery(this).is(':checked')){
			console.log(checkbox_val);
			var data_row = [
				hover_data[0],
				hover_data[1],
				hover_data[2],
				hover_data[3],
				hover_data[4],
				hover_data[5],
				hover_data[6],
				hover_data[7],
				hover_data[8]
			];
			
			var rowNode = compare_table
			.row.add(data_row)
			.draw()
			.nodes()
			.to$()
			.addClass( 'tr-post-id-' + checkbox_val )
			.find('.loopDT')
			.prop('checked', true);
			
			jQuery('#loopDatatables').find('.chk-post-id-' + checkbox_val).prop('checked', true);
			ljc_count_compare_table();
			jQuery( document.body ).trigger( 'add_comparison_table', [data]);
		}else{
			jQuery('#loopDatatables').find('.chk-post-id-' + checkbox_val).prop('checked', false);
			compare_table.row('.tr-post-id-' + checkbox_val).remove().draw();
			ljc_count_compare_table();
			jQuery( document.body ).trigger( 'remove_comparison_table', [data]);
		}
	});
};
var compareDataTable = function(dtOption, compare_table, init_hover){
	jQuery(document).on('hover','#compareDataTable tbody tr',function(e){
		var data = compare_table.row( this ).data();
		var post_id = jQuery(this).data('post-id');

		right_noinfo();
		right_info(true);

		hover_data = data;
		init_hover(post_id, hover_data);
	});
	jQuery(document).on('click','#compareDataTable tbody .loopDT',function(e){
		var checkbox_val = jQuery(this).val();
		var data = {
			id:checkbox_val
		}
		if(jQuery(this).is(':checked')){
			console.log('.chk-post-id-' + checkbox_val);
		}else{
			jQuery('#loopDatatables').find('.chk-post-id-' + checkbox_val).prop('checked', false);
			jQuery('#loopRecentViewDatatables').find('.chk-post-id-' + checkbox_val).prop('checked', false);
			compare_table.row('.tr-post-id-' + checkbox_val).remove().draw( false );
			ljc_count_compare_table();
			jQuery( document.body ).trigger( 'remove_comparison_table', [data]);
		}
	});
	jQuery('#compareDataTable tbody .loopDT').attr('checked',true);
};
	
//datatables style
function datatables_style(){
	jQuery('.e3ve-datatables-container .dataTables_scroll').find('.dataTables_scrollFoot').remove();
	if ( jQuery('.e3ve-datatables-container .dataTables_scrollBody').length ) {
	jQuery('.e3ve-datatables-container .dataTables_scrollBody').css({'height': '308px', 'border-bottom': '1px solid #ccc', 'max-height': 'none'});	
	}
	
	jQuery('.dataTables_scrollHeadInner').css('width', '');
	jQuery('.dataTables_scrollHeadInner table').css('width', '');
	jQuery('.dataTables_scrollBody').css('border-bottom', '');
	jQuery(document).ajaxComplete(function() {
		jQuery('.dataTables_scrollHeadInner').css('width', '');
		jQuery('.dataTables_scrollHeadInner table').css('width', '');
		jQuery('.dataTables_scrollBody').css('border-bottom', '');
	});
	
	jQuery('.dataTables_scrollHeadInner table thead tr th').css('width', '');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(0).css('width','11%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(1).css('width','11%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(2).css('width','9%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(3).css('width','9%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(4).css('width','11%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(5).css('width','15%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(6).css('width','11%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(7).css('width','14%');
	//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(8).css('width','9%');
	
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(0).css('width','11%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(1).css('width','11%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(2).css('width','9%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(3).css('width','9%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(4).css('width','11%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(5).css('width','15%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(6).css('width','11%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(7).css('width','14%');
	//jQuery('.dataTables_scrollBody table tbody tr td').eq(8).css('width','9%');
	
	jQuery(document).ajaxComplete(function(){
		jQuery('.dataTables_scrollHeadInner table thead tr th').css('width', '');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(0).css('width','11%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(1).css('width','11%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(2).css('width','9%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(3).css('width','9%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(4).css('width','11%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(5).css('width','15%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(6).css('width','11%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(7).css('width','14%');
		//jQuery('.dataTables_scrollHeadInner table thead tr th').eq(8).css('width','9%');
		
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(0).css('width','11%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(1).css('width','11%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(2).css('width','9%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(3).css('width','9%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(4).css('width','11%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(5).css('width','15%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(6).css('width','11%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(7).css('width','14%');
		//jQuery('.dataTables_scrollBody table tbody tr td').eq(8).css('width','9%');
	});
	jQuery(document).ajaxComplete(function(){
		if(jQuery('.dataTables_scrollHeadInner table thead tr th').length ) {
			jQuery('.dataTables_scrollHeadInner table thead tr th').css().remove();
		}
	});
	//if( ! jQuery('.dataTables_scrollHeadInner table thead tr th').css('width', '').length ){
	//	jQuery('.dataTables_scrollHeadInner table thead tr th').css('width', '');
	//}
	
	jQuery('#CFDR #loopDatatables_wrapper #loopDatatables_filter').remove();
	jQuery('#RV #loopRecentViewDatatables_wrapper #loopRecentViewDatatables_filter').remove();
	jQuery('#Comp #compareDataTable_wrapper #compareDataTable_filter').remove();
	
	
	
}
jQuery(document).ready(function() {
	right_info();
	right_noinfo(true);
	function openD(evt, dResults) {
		// Declare all variables
		var i, tabcontent, tablinks;

		// Get all elements with class="tabcontent" and hide them
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}

		// Get all elements with class="tablinks" and remove the class "active"
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}

		// Show the current tab, and add an "active" class to the link that opened the tab
		document.getElementById(dResults).style.display = "block";
		evt.currentTarget.className += " active";
	}
	function openImage(evt, imageName) {
		// Declare all variables
		var i, e3veImagee3veImageTabcontent, e3veImagee3veImageTablinks;

		// Get all elements with class="e3veImagee3veImageTabcontent" and hide them
		e3veImagee3veImageTabcontent = document.getElementsByClassName("e3veImagee3veImageTabcontent");
		for (i = 0; i < e3veImagee3veImageTabcontent.length; i++) {
			e3veImagee3veImageTabcontent[i].style.display = "none";
		}

		// Get all elements with class="e3veImagee3veImageTablinks" and remove the class "active"
		e3veImagee3veImageTablinks = document.getElementsByClassName("e3veImagee3veImageTablinks");
		for (i = 0; i < e3veImagee3veImageTablinks.length; i++) {
			e3veImagee3veImageTablinks[i].className = e3veImagee3veImageTablinks[i].className.replace(" active", "");
		}

		// Show the current e3veImageTab, and add an "active" class to the link that opened the e3veImageTab
		document.getElementById(imageName).style.display = "block";
		evt.currentTarget.className += " active";
	}
	
	diamondTable(options_share_datatable, diamond_table, compare_table, init_hover);
	reviewDataTable(options_share_datatable, recent_view_table, compare_table, init_hover);
	compareDataTable(options_share_datatable, compare_table, init_hover);
	datatables_style();
});

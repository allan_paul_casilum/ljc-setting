//jQuery( window ).bind("load", function() {
jQuery(document).ready(function(){
	// BIG CHEAT - Remove term-26 so that most of the code for settings won't execute
    if ( jQuery( 'body').hasClass( 'term-26' ) ) {
        jQuery("body").removeClass('term-26');
        jQuery("body").addClass('term-26-placeholder');
    }
//	jQuery("body").removeClass('term-1326');
	
    //Page Social Links Button
    jQuery( ".e3ve-social-icons-bottom" ).clone().appendTo( ".post-content" );

    //Hide reset filter icon/link
    jQuery(".prdctfltr_filter_inner").find(".prdctfltr_clear").remove();
	
	// Make sure that there is always a selected item in the category filter
	jQuery(".prdctfltr_cat").find("label").click(function() {
		if(this.firstChild.checked) return false;
		return true;
	});
	

    //Search Filter
if (false) {
    if ( ! jQuery('#1326-wrapper-1').length ) {
        jQuery( ".term-1326 .prdctfltr_pa_diamond-shape, .term-1326 .prdctfltr_pa_carat-weight" ).wrapAll( "<div id='1326-wrapper-1' class='e3ve-diamond-wrapper-1'></div>" );
        jQuery( ".term-1326 .prdctfltr_price, .term-1326 .prdctfltr_pa_diamond-cut, .term-1326 .prdctfltr_pa_center-stone-clarity, .term-1326 .prdctfltr_pa_diamond-color" ).wrapAll( "<div id='1326-wrapper-2' class='e3ve-diamond-wrapper-2'></div>" );
    }
    if ( ! jQuery('#26-wrapper-1').length ) {
        jQuery( ".term-26 .prdctfltr_pa_metal .prdctfltr_checkboxes label.prdctfltr_ft_rose_gold, div[data-sub='rose_gold']" ).wrapAll( "<div id='#26-wrapper-1' class='e3ve-expandable-wrapper-1'></div>" );
        jQuery( ".term-26 .prdctfltr_pa_metal .prdctfltr_checkboxes label.prdctfltr_ft_white_gold, div[data-sub='white_gold']" ).wrapAll( "<div id='#26-wrapper-1' class='e3ve-expandable-wrapper-1'></div>" );
        jQuery( ".term-26 .prdctfltr_pa_metal .prdctfltr_checkboxes label.prdctfltr_ft_yellow_gold, div[data-sub='yellow_gold']" ).wrapAll( "<div id='#26-wrapper-1' class='e3ve-expandable-wrapper-1'></div>" );
    }
}	

    //Breadcrumb-steps
    jQuery(".e3ve-breadcrumbs-step-1 p.p-2").parent().addClass('e3ve-middle');
    jQuery(".e3ve-breadcrumbs-step-1 p.p-3").parent().addClass('e3ve-middle');

    //Hide filters everywhere except on the settings or diamonds category pages
    //var pathname = window.location.pathname;
    //if (pathname != '/product-category/settings/' && pathname != '/product-category/diamonds/') {
    //  jQuery( '.prdctfltr_wc.prdctfltr_woocommerce.woocommerce.prdctfltr_wc_regular.pf_default.prdctfltr_always_visible.prdctfltr_click_filter.prdctfltr_rows.pf_mod_multirow.pf_adptv_default.prdctfltr_checkbox.prdctfltr_hierarchy_lined.pf_remove_clearall' ).css('display', 'none');
    //}

    //jQuery for Hide and Show
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-17 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-26-placeholder .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-42 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-49 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-630 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-574 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-1320 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-1321 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-1325 .prdctfltr_wc" );
    jQuery( "<div class='e3ve-show-hide' style='color: rgb(255, 255, 255); text-align: center; text-transform: uppercase; margin: 0px auto 20px; background: transparent none repeat scroll 0px 0px; border-top: 2px solid rgb(70, 90, 123); line-height: normal;'><span style='background: rgb(70, 90, 123) none repeat scroll 0% 0%; cursor: pointer; line-height: 22px; display: block; width: 150px; margin: 0px auto; font-size: 12px;'>Hide</span></div>" ).insertAfter( ".term-1326 .prdctfltr_wc" );

    set_show_hide_text();

    jQuery(".e3ve-show-hide span").click(function(){
        show_hide_filters();

        //jQuery(".term-1326 .prdctfltr_wc").slideToggle(400, function() {
        //    if ( jQuery(this).is(':visible') ) {
        //        jQuery('.e3ve-show-hide span').text('hide');
        //    } else {
        //        jQuery('.e3ve-show-hide span').text('show');
        //    }
        //});
    });

    //Selection Dropdown Hide and Show
    //jQuery('body').click(function() {
    //    var myTarget = jQuery('.e3ve-inner-selection');
    //    if (myTarget.is(':visible') ) {
    //        myTarget.slideToggle();
    //    }
    //});
    //jQuery(".e3ve-inner-selection").click(function(e){
    //    e.stopPropagation();
    //});
    //jQuery(".e3ve-selection-dropdown-inner").click(function(e){
    //    e.stopPropagation();
    //    jQuery(".e3ve-inner-selection").slideToggle();
    //});
    //jQuery("#reset_selections").click(function(e){
    //    e.stopPropagation();
    //    reset_breadcrumb_selections();
    //});
    //jQuery("#manage_selections").click(function(e){
    //    e.stopPropagation();
    //    jQuery('#myModal').modal('toggle');
    //});

    //Facilitate show/hide from product filter menu bars
    jQuery('.prdctfltr-bars').css('cursor', 'pointer').click(function() {
        show_hide_filters();
    });

    //jQuery( 'span.prdctfltr_reset label' ).unbind('click').on('click', function() {
    //jQuery( 'span.prdctfltr_reset label' ).off('click').on('click', function() {
    //jQuery( 'span.prdctfltr_reset' ).unbind('click').on('click', function() {
    //jQuery( 'span.prdctfltr_reset' ).off('click').on('click', function() {
    //jQuery( '.prdctfltr_buttons input[name="reset_filter"]' ).unbind('click').on('click', function() {
    //jQuery( '.prdctfltr_buttons input[name="reset_filter"], span.prdctfltr_reset, span.prdctfltr_reset label' ).off('click').on('click', function() {
    //jQuery( '.prdctfltr_buttons' ).off('click').on('click', function() {
    //    console.log('reset filter clicked')
    //});

    //jQuery( '.prdctfltr_buttons' ).after( '<button type="reset" value="Reset Filters" id="filter_reset">Reset Filters</button>' );
    //jQuery( '.prdctfltr_buttons' ).after( '<a id="filter_reset">Reset Filters</a>' );
    //jQuery( '#prdctfltr_rng_57e89f9561dac5').ionRangeSlider({
    //var sliderTarget = jQuery( '.pf_rng_pa_center-stone-minimum-size .irs-hidden-input');
    //var sliderId = sliderTarget.id;
    //console.log(sliderId);
    //jQuery( '.pf_rng_pa_center-stone-minimum-size .irs-hidden-input').ionRangeSlider({
    ////jQuery( 'span.irs.js-irs-47').ionRangeSlider({
    //    min: 10,
    //    max: 50,
    //    grid: true,
    //    force_edges: true
    //});
});

jQuery( document ).ajaxComplete(function() {
    jQuery(".prdctfltr_filter_inner").find(".prdctfltr_clear").remove();

	// Make sure that there is always a selected item in the category filter
	jQuery(".prdctfltr_cat").find("label").click(function() {
		if(this.firstChild.checked) return false;
		return true;
	});

if (false) {	
    if ( ! jQuery('#1326-wrapper-1').length ) {
        jQuery( ".term-1326 .prdctfltr_pa_diamond-shape, .term-1326 .prdctfltr_pa_carat-weight" ).wrapAll( "<div id='1326-wrapper-1' class='e3ve-diamond-wrapper-1'></div>" );
        jQuery( ".term-1326 .prdctfltr_price, .term-1326 .prdctfltr_pa_diamond-cut, .term-1326 .prdctfltr_pa_center-stone-clarity, .term-1326 .prdctfltr_pa_diamond-color" ).wrapAll( "<div id='1326-wrapper-2' class='e3ve-diamond-wrapper-2'></div>" );
    }

    if ( ! jQuery('#26-wrapper-1').length ) {
        jQuery( ".term-26 .prdctfltr_pa_metal .prdctfltr_checkboxes label.prdctfltr_ft_rose_gold, div[data-sub='rose_gold']" ).wrapAll( "<div id='#26-wrapper-1' class='e3ve-expandable-wrapper-1'></div>" );
        jQuery( ".term-26 .prdctfltr_pa_metal .prdctfltr_checkboxes label.prdctfltr_ft_white_gold, div[data-sub='white_gold']" ).wrapAll( "<div id='#26-wrapper-1' class='e3ve-expandable-wrapper-1'></div>" );
        jQuery( ".term-26 .prdctfltr_pa_metal .prdctfltr_checkboxes label.prdctfltr_ft_yellow_gold, div[data-sub='yellow_gold']" ).wrapAll( "<div id='#26-wrapper-1' class='e3ve-expandable-wrapper-1'></div>" );
    }
}	

    jQuery('.prdctfltr-bars' ).css('cursor', 'pointer').click(function() {
        show_hide_filters();
    });

    //jQuery( '.prdctfltr_woocommerce_filter .pf_ajax_circles' ).css('cursor', 'pointer');
    //jQuery( '.prdctfltr-bars' ).css('cursor', 'pointer');

    //jQuery( '.prdctfltr_buttons' ).after( '<button type="reset" value="Reset Filters" id="filter_reset">Reset Filters</button>' );
    //jQuery( '.prdctfltr_buttons' ).after( '<input type="reset">' );

    //**** Hide price slider when there are no products ****//
    var no_product_text = jQuery( '.woocommerce-info' ).html();
    if (no_product_text == 'No products were found matching your selection.') {
        jQuery( '.prdctfltr_price').remove();
    }
});

//jQuery( '#filter_reset').on('click', function() {
//    alert('debug');
//});

//Selection Dropdown Hide and Show (alternate version)
jQuery(document).click(function(e){
    var targetId    = 'your_selections_dropdown';
    var targetClass = 'e3ve-selection-dropdown';
    var myTarget    = jQuery('#' + targetId);
    var myClass     = e.target.className;
    var myDisplay   = e.target.style.display;
    var myId        = e.target.id;

    if ( (myClass != targetClass) && (myId != targetId) ) {
        if (jQuery('.e3ve-inner-selection').is(':visible')) {
            jQuery('.e3ve-inner-selection').slideToggle();
        }
    } else {
        jQuery(".e3ve-inner-selection").slideToggle();
    }
});

function set_show_hide_text() {
    if ( jQuery('.e3ve-show-hide').is(':visible') ) {
        jQuery('.e3ve-show-hide span').text('hide');
    } else {
        jQuery('.e3ve-show-hide span').text('show filters');
    }
}

function show_hide_filters() {
    jQuery(
        ".term-17 .prdctfltr_wc," +
        ".term-26-placeholder .prdctfltr_wc," +
        ".term-42 .prdctfltr_wc," +
        ".term-49 .prdctfltr_wc," +
        ".term-630 .prdctfltr_wc," +
        ".term-574 .prdctfltr_wc," +
        ".term-1320 .prdctfltr_wc," +
        ".term-1321 .prdctfltr_wc," +
        ".term-1325 .prdctfltr_wc," +
        ".term-1326 .prdctfltr_wc"
    ).slideToggle(400, function() {
            if ( jQuery(this).is(':visible') ) {
                jQuery('.e3ve-show-hide span').text('hide');
            } else {
                jQuery('.e3ve-show-hide span').text('show filters');
            }
        });
}

//move category pages sidebar before content
jQuery(document).ready(function(){
    jQuery( ".tax-product_cat #sidebar" ).insertBefore( ".tax-product_cat #content" );
    jQuery( ".single-product #sidebar" ).insertBefore( ".single-product #content" );
    jQuery( "#sidebar" ).addClass( "e3ve-product-cat-sidebar" );
});

//remove unused filter in settings page
jQuery().ready(function(){
	jQuery('.term-26 .prdctfltr_search').remove();
    jQuery('.term-26 .prdctfltr_price').remove();
    jQuery('.term-26 .prdctfltr_pa_diamond-cut').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-clarity').remove();
    jQuery('.term-26 .prdctfltr_pa_diamond-color').remove();
    jQuery('.term-26 .prdctfltr_orderby').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-shape').remove();
    jQuery('.term-26 .prdctfltr_pa_carat-weight').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-minimum-size').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-maximum-size').remove();
});
jQuery( document ).ajaxComplete(function() {
if ( jQuery('.prdctfltr_search'||'.prdctfltr_price'||'.prdctfltr_pa_diamond-cut'||'.prdctfltr_pa_center-stone-clarity'||'.prdctfltr_pa_diamond-color'||'.prdctfltr_orderby'||'.prdctfltr_pa_center-stone-shape'||'.prdctfltr_pa_center-stone-shape'||'.prdctfltr_pa_carat-weight'||'.prdctfltr_pa_center-stone-minimum-size'||'.prdctfltr_pa_center-stone-maximum-size').length ) {
	jQuery('.term-26 .prdctfltr_search').remove();
    jQuery('.term-26 .prdctfltr_price').remove();
    jQuery('.term-26 .prdctfltr_pa_diamond-cut').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-clarity').remove();
    jQuery('.term-26 .prdctfltr_pa_diamond-color').remove();
    jQuery('.term-26 .prdctfltr_orderby').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-shape').remove();
    jQuery('.term-26 .prdctfltr_pa_carat-weight').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-minimum-size').remove();
    jQuery('.term-26 .prdctfltr_pa_center-stone-maximum-size').remove();
}
});
//remove unused filter in diamond page
jQuery(document).ready(function(){
    jQuery('.term-1326 .prdctfltr_attributes .prdctfltr_cat').remove();
    jQuery('.term-1326 .prdctfltr_pa_new-or-vintage').remove();
    jQuery('.term-1326 .prdctfltr_pa_design-collection').remove();
    jQuery('.term-1326 .prdctfltr_pa_metal').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-shape').remove();
    jQuery('.term-1326 .prdctfltr_search').remove();
    jQuery('.term-1326 .prdctfltr_orderby').remove();
    jQuery('.term-1326 .prdctfltr_pa_design-era').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-minimum-size').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-maximum-size').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-size').remove();
    jQuery('.catalog-ordering').remove();//Remove 'sort by price' and 'show X products' bar
});
jQuery( document ).ajaxComplete(function() {
if ( jQuery('.prdctfltr_attributes prdctfltr_cat', '.prdctfltr_pa_new-or-vintage', '.prdctfltr_pa_design-collection', '.prdctfltr_pa_metal', '.prdctfltr_pa_center-stone-shape', '.prdctfltr_search', '.prdctfltr_orderby', '.prdctfltr_pa_design-era', '.prdctfltr_pa_center-stone-minimum-size', '.prdctfltr_pa_center-stone-maximum-size', '.prdctfltr_pa_center-stone-size').length){
    jQuery('.term-1326 .prdctfltr_attributes .prdctfltr_cat').remove();
    jQuery('.term-1326 .prdctfltr_pa_new-or-vintage').remove();
    jQuery('.term-1326 .prdctfltr_pa_design-collection').remove();
    jQuery('.term-1326 .prdctfltr_pa_metal').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-shape').remove();
    jQuery('.term-1326 .prdctfltr_search').remove();
    jQuery('.term-1326 .prdctfltr_orderby').remove();
    jQuery('.term-1326 .prdctfltr_pa_design-era').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-minimum-size').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-maximum-size').remove();
    jQuery('.term-1326 .prdctfltr_pa_center-stone-size').remove();
    jQuery('.catalog-ordering').remove();//Remove 'sort by price' and 'show X products' bar
}
});
//remove unused filters in other category pages
jQuery(document).ready(function(){
    if (window.location.pathname == "/product-category/settings/") {
        jQuery('.prdctfltr_pa_center-stone-size').show();
    } else {
        jQuery('.prdctfltr_pa_center-stone-size').remove();
    }
});
jQuery( document ).ajaxComplete(function() {
if ( jQuery('.prdctfltr_pa_center-stone-size').length){
    if (window.location.pathname == "/product-category/settings/") {
        jQuery('.prdctfltr_pa_center-stone-size').show();
    } else {
        jQuery('.prdctfltr_pa_center-stone-size').remove();
    }
}
});

//moving breadcrumbs
jQuery(document).ready(function(){
	jQuery('.e3ve-breadcrumbs').insertBefore('#ljc_subheading');
	jQuery('.e3ve-breadcrumbs').prependTo('#content');
});

//**** Hide price slider when there are no products ****//
jQuery(document).ready(function() {
    var no_product_text = jQuery( '.woocommerce-info' ).html();
    if (no_product_text == 'No products were found matching your selection.') {
        jQuery( '.prdctfltr_price').remove();
    }
});

//DATATABLES
function openD(evt, dResults) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(dResults).style.display = "block";
    evt.currentTarget.className += " active";
}
function openImage(evt, imageName) {
    // Declare all variables
    var i, e3veImagee3veImageTabcontent, e3veImagee3veImageTablinks;

    // Get all elements with class="e3veImagee3veImageTabcontent" and hide them
    e3veImagee3veImageTabcontent = document.getElementsByClassName("e3veImagee3veImageTabcontent");
    for (i = 0; i < e3veImagee3veImageTabcontent.length; i++) {
        e3veImagee3veImageTabcontent[i].style.display = "none";
    }

    // Get all elements with class="e3veImagee3veImageTablinks" and remove the class "active"
    e3veImagee3veImageTablinks = document.getElementsByClassName("e3veImagee3veImageTablinks");
    for (i = 0; i < e3veImagee3veImageTablinks.length; i++) {
        e3veImagee3veImageTablinks[i].className = e3veImagee3veImageTablinks[i].className.replace(" active", "");
    }

    // Show the current e3veImageTab, and add an "active" class to the link that opened the e3veImageTab
    document.getElementById(imageName).style.display = "block";
    evt.currentTarget.className += " active";
}

// DATATABLES BODY STYLE
jQuery(document).ready(function() {
jQuery('.e3ve-datatables-container .dataTables_scroll').find('.dataTables_scrollFoot').remove();
if ( jQuery('.e3ve-datatables-container .dataTables_scrollBody').length ) {
jQuery('.e3ve-datatables-container .dataTables_scrollBody').css({'width': '294px', 'border-bottom': '1px solid #ccc'});	
}
});

jQuery(document).ready(function() {
    jQuery('prdctfltr_cat_support').siblings('img').hide();
});

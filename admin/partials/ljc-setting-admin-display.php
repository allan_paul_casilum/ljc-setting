<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       apysais.com
 * @since      1.0.0
 *
 * @package    Ljc_Setting
 * @subpackage Ljc_Setting/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

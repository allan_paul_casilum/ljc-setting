/**
 * Callback function for the 'click' event of the 'Set Footer Image'
 * anchor in its meta box.
 *
 * Displays the media uploader for selecting an image.
 *
 * @since 0.1.0
 */
function renderMediaUploader(_loop_id, _loop_type) {
    'use strict';
  
    var file_frame, image_data;
	var ljc_variant_loop_id = 0;
	var wrapper = jQuery( '#woocommerce-product-data' );
    /**
     * If an instance of file_frame already exists, then we can open it
     * rather than creating a new instance.
     */
    if ( undefined !== file_frame ) {
 
        file_frame.open();
        return;
 
    }
 
    /**
     * If we're this far, then an instance does not exist, so we need to
     * create our own.
     *
     * Here, use the wp.media library to define the settings of the Media
     * Uploader. We're opting to use the 'post' frame which is a template
     * defined in WordPress core and are initializing the file frame
     * with the 'insert' state.
     *
     * We're also not allowing the user to select more than one image.
     */
    file_frame = wp.media.frames.file_frame = wp.media({
        title: 'Choose image',
		button: {
			text: 'Use Image'
		},
		multiple: false
    });
 
    /**
     * Setup an event handler for what to do when an image has been
     * selected.
     *
     * Since we're using the 'view' state when initializing
     * the file_frame, we need to make sure that the handler is attached
     * to the insert event.
     */
    file_frame.on( 'select', function() {
		var ljc_metal_view_img, ljc_metal_url;
		// Get media attachment details from the frame state
		var attachment = file_frame.state().get('selection').first().toJSON();
		// here are some of the variables you could use for the attachment;
		//var all = JSON.stringify( attachment );      
		var id = attachment.id;
		//var title = attachment.title;
		//var filename = attachment.filename;
		var url = attachment.url;
		//var link = attachment.link;
		//var alt = attachment.alt;
		//var author = attachment.author;
		//var description = attachment.description;
		//var caption = attachment.caption;
		//var name = attachment.name;
		//var status = attachment.status;
		//var uploadedTo = attachment.uploadedTo;
		//var date = attachment.date;
		//var modified = attachment.modified;
		//var type = attachment.type;
		//var subtype = attachment.subtype;
		//var icon = attachment.icon;
		//var dateFormatted = attachment.dateFormatted;
		//var editLink = attachment.editLink;
		//var fileLength = attachment.fileLength;
        ljc_metal_view_img = _loop_type + '-img-id-' + _loop_id;
        ljc_metal_url = _loop_type + '-url-img-' + _loop_id;
		jQuery("." + ljc_metal_view_img).val(id);
		jQuery("." + ljc_metal_url).val(url);
		jQuery("." + _loop_type + '-img-postid-' + _loop_id).val(id);
    });
 
    // Now display the actual file_frame
    file_frame.open();
 
}
function ljc_remove_img(_loop_id, _loop_type){
	'use strict';
	var ljc_metal_view_img, ljc_metal_url;
	
	ljc_metal_view_img = _loop_type + '-img-id-' + _loop_id;
	ljc_metal_url = _loop_type + '-url-img-' + _loop_id;
	jQuery("." + ljc_metal_view_img).val(0);
	jQuery("." + ljc_metal_url).val('');
	jQuery("." + _loop_type + '-img-postid-' + _loop_id).val(0);
	jQuery("." + _loop_type + '-img-uploaded-' + _loop_id).hide();
}
(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$(function() {
        $(document).on('click', '.ljc-upload-img', function( evt ) {
            // Stop the anchor's default behavior
            evt.preventDefault();
			var ljc_loop_id = 0;
			var ljc_loop_type = '';
			ljc_loop_id = $(this).data('loop-id');
			ljc_loop_type = $(this).data('loop-img');
            // Display the media uploader
            renderMediaUploader(ljc_loop_id, ljc_loop_type);
        });
        $(document).on('click', '.ljc-delete-img', function( evt ) {
            // Stop the anchor's default behavior
            evt.preventDefault();
			var ljc_loop_id = 0;
			var ljc_loop_type = '';
			ljc_loop_id = $(this).data('loop-id');
			ljc_loop_type = $(this).data('loop-img');
			ljc_remove_img(ljc_loop_id, ljc_loop_type);
        });
    });
})( jQuery );

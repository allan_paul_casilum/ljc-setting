<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              apysais.com
 * @since             1.0.0
 * @package           Ljc_Setting
 *
 * @wordpress-plugin
 * Plugin Name:       Leighvintage Jewerly
 * Plugin URI:        techriver.net
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Allan Paul Casilum
 * Author URI:        apysais.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ljc-setting
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
$GLOBALS['show_breadcrumb'] = false;

spl_autoload_register('ljc_autoload_class');
function ljc_autoload_class($class_name){
	if ( false !== strpos( $class_name, 'LJC' ) ) {
		$include_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
		$admin_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR;
		$class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
		if( file_exists($include_classes_dir . $class_file) ){
			require_once $include_classes_dir . $class_file;
		}
		if( file_exists($admin_classes_dir . $class_file) ){
			require_once $admin_classes_dir . $class_file;
		}
	}
}
/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {//check if woo is active
	require_once(ABSPATH.'wp-admin/includes/plugin.php');
    function mwp_config_ljc(){
		$plugin_data = get_plugin_data( __FILE__ );
		/*
		 * config
		 * */
		$config_ljs = array();
		$config_ljs['localize_domain'] = 'ljc-setting';
		$config_ljs['plugin_data'] = $plugin_data;
		$config_ljs['plugin_path'] = array(
			'root_dir' => plugin_dir_path( __FILE__ ),
			'admin_dir' => plugin_dir_path( __FILE__ ) .  'admin/',
			'admin_dir_partials' => plugin_dir_path( __FILE__ ) .  'admin/partials/',
			'admin_url_path' => plugin_dir_url( __FILE__ ) .  'admin/',
			'plugin_dir_path' => plugin_dir_path( __FILE__ ),
			'plugin_url_path' => plugin_dir_url( __FILE__ ) . 'public/',
			'public_partials' => plugin_dir_path( __FILE__ ) .  'public/partials/',
			'public_url_partials' => plugin_dir_url( __FILE__ ) .  'public/partials/',
			'theme_view' => get_stylesheet_directory(),
			'plugin_folder_name' => $plugin_data['TextDomain'],
			'asset_dir' => plugin_dir_path( __FILE__ ) .  'assets/',
		);
		$config_ljs['url'] = array(
			'complete_your_ring' =>  home_url( '/complete-your-ring/' ),
		);
		$config_ljs['woo_variations'] = array(
			'pa_prefix' => 'pa_metal'
		);
		//attribute_pa_ring-size
		$config_ljs['ring_size'] = array(
			'pa_prefix' => 'pa_size'
		);
		$config_ljs['woo_categories'] = array(
			'setting' => 'settings',
			'diamond' => 'diamonds'
		);
		$config_ljs['woo_attribute_prefix'] = array(
			'pa_attribute' => 'attribute_' . $config_ljs['woo_variations']['pa_prefix']
		);
		$config_ljs['title_heading'] = array(
			'engagement_array' => array(
                'engagement-rings' => 'Engagement Rings'
			),
			'wedding_ring' => array(
				'wedding-rings' => 'Wedding Rings'
			),
			'jewerly_ring' => array(
				'jewelry' => 'Jewelry'
			)
		);
		$config_ljs['verbage'] = array(
			'cart_title' => __('Your Selections', $config_ljs['localize_domain']),
			'setting_name' => __('Settings', $config_ljs['localize_domain']),
			'no_setting_data' => __('No Settings Data', $config_ljs['localize_domain']),
			'no_setting_selected' => __('No Settings Selected', $config_ljs['localize_domain']),
			'diamond_name' => __('Diamonds', $config_ljs['localize_domain']),
			'no_diamond_data' => __('No Diamond Data', $config_ljs['localize_domain']),
			'no_diamond_selected' => __('No Diamonds Selected', $config_ljs['localize_domain']),
			'selections_duplicate' => __('Duplicate diamond and setting selection - please save a different pair', $config_ljs['localize_domain']),
			'selections_success' => __('Diamond and setting pair saved', $config_ljs['localize_domain']),
			'current_save_selections' => __('Current Saved Selections', $config_ljs['localize_domain']),
			'ajax_save_selection_success' => __('Saving your selected setting and diamond pair...',$config_ljs['localize_domain']),
			'temp_cart_add_to_cart_button' => __('Add To Cart',$config_ljs['localize_domain']),
			'temp_cart_remove_to_cart_button' => __('Remove Saved Selection',$config_ljs['localize_domain']),
			'temp_cart_total' => __('Total',$config_ljs['localize_domain']),
			'temp_cart_total_loop_price' => __('Price',$config_ljs['localize_domain']),
			'ajax_temp_cart_add_to_wc_cart' => __('Adding custom ring to cart...',$config_ljs['localize_domain']),
			'ajax_temp_cart_added_to_wc_cart' => __('Added custom ring to cart',$config_ljs['localize_domain']),
			'ajax_temp_cart_added_to_wc_cart_fail' => __('Error adding custom ring to cart - problem with one or both selections',$config_ljs['localize_domain']),
			'ajax_temp_cart_getting_saved_selections_container' => __('Getting Saved Selections',$config_ljs['localize_domain']),
			'ajax_temp_cart_successfully_delete_item' => __('Successfully Deleted Item',$config_ljs['localize_domain']),
			'ajax_temp_cart_deleting_item' => __('Deleting Item',$config_ljs['localize_domain']),
			'remove_this_setting_item' => __('Remove this Setting',$config_ljs['localize_domain']),
			'remove_this_diamond_item' => __('Remove this Diamond',$config_ljs['localize_domain']),
			'ajax_remove_this_diamond_item_progress' => __('Removing this diamond...',$config_ljs['localize_domain']),
			'ajax_remove_this_setting_item_progress' => __('Removing this setting...',$config_ljs['localize_domain']),
			'ajax_remove_this_diamond_item_success' => __('Successfully Removed Diamond',$config_ljs['localize_domain']),
			'ajax_remove_this_setting_item_success' => __('Successfully Removed Setting',$config_ljs['localize_domain']),
			'ajax_remove_this_diamond_item_fail' => __('Failed Removing Diamond',$config_ljs['localize_domain']),
			'ajax_remove_this_setting_item_fail' => __('Failed Removing Setting',$config_ljs['localize_domain']),
			'empty_saved_selection' => __('Empty Saved Selections',$config_ljs['localize_domain']),
			'saved_selection' => __('Save Selections',$config_ljs['localize_domain']),
			'add_to_cart_selection' => __('Add Selections to Cart',$config_ljs['localize_domain']),
			'empty_selection' => __('Remove All Selections',$config_ljs['localize_domain']),
			'settings_product' => __('Design Your Ring',$config_ljs['localize_domain']),
			'setting_single' => __('Engagement Ring',$config_ljs['localize_domain']),
			'diamond_single' => __('Diamond',$config_ljs['localize_domain']),
			'engagement_cat' => __('All Engagement Rings',$config_ljs['localize_domain']),
			'engagement_product' => __('Engagement Rings',$config_ljs['localize_domain']),
			'wedding_product' => __('Wedding Rings',$config_ljs['localize_domain']),
			'jewerly_product' => __('Jewelry',$config_ljs['localize_domain']),
			'subheading_settings' => __('Choose Your Setting',$config_ljs['localize_domain']),
			'subheading_diamonds' => __('Choose Your Diamond',$config_ljs['localize_domain']),
		);
		$config_ljs['localize'] = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'security' => wp_create_nonce( 'md-ajax-request' ),
			'ajax_save_selection_msg_success' => $config_ljs['verbage']['ajax_save_selection_success'],
			'ajax_temp_cart_add_to_wc_cart' => $config_ljs['verbage']['ajax_temp_cart_add_to_wc_cart'],
			'ajax_temp_cart_added_to_wc_cart' => $config_ljs['verbage']['ajax_temp_cart_added_to_wc_cart'],
			'ajax_temp_cart_added_to_wc_cart_fail' => $config_ljs['verbage']['ajax_temp_cart_added_to_wc_cart_fail'],
			'ajax_temp_cart_getting_saved_selections_container' => $config_ljs['verbage']['ajax_temp_cart_getting_saved_selections_container'],
			'ajax_temp_cart_successfully_delete_item' => $config_ljs['verbage']['ajax_temp_cart_successfully_delete_item'],
			'ajax_temp_cart_deleting_item' => $config_ljs['verbage']['ajax_temp_cart_deleting_item'],
			'ajax_remove_this_diamond_item_progress' => $config_ljs['verbage']['ajax_remove_this_diamond_item_progress'],
			'ajax_remove_this_setting_item_progress' => $config_ljs['verbage']['ajax_remove_this_setting_item_progress'],
			'ajax_remove_this_diamond_item_success' => $config_ljs['verbage']['ajax_remove_this_diamond_item_success'],
			'ajax_remove_this_setting_item_success' => $config_ljs['verbage']['ajax_remove_this_setting_item_success'],
			'ajax_remove_this_diamond_item_fail' => $config_ljs['verbage']['ajax_remove_this_diamond_item_fail'],
			'ajax_remove_this_setting_item_fail' => $config_ljs['verbage']['ajax_remove_this_setting_item_fail']
		 );
		return $config_ljs;
		//config//
	}
	//check if WP_Session plugin installed
	function is_wp_session_manager_install(){
		if( in_array( 
				'wp-session-manager/wp-session-manager.php', 
				apply_filters( 'active_plugins', get_option( 'active_plugins' )	)
			) 
		){
			return true;
		}
		return false;
	}
    /**
	 * The code that runs during plugin activation.
	 * This action is documented in includes/class-ljc-setting-activator.php
	 */
	function activate_ljc_setting() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-ljc-setting-activator.php';
		Ljc_Setting_Activator::activate();
	}

	/**
	 * The code that runs during plugin deactivation.
	 * This action is documented in includes/class-ljc-setting-deactivator.php
	 */
	function deactivate_ljc_setting() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-ljc-setting-deactivator.php';
		Ljc_Setting_Deactivator::deactivate();
	}

	register_activation_hook( __FILE__, 'activate_ljc_setting' );
	register_deactivation_hook( __FILE__, 'deactivate_ljc_setting' );

	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	 */
	require plugin_dir_path( __FILE__ ) . 'includes/class-ljc-setting.php';
	//functions
	require plugin_dir_path( __FILE__ ) . 'inc-functions.php';

	/**
	 * Begins execution of the plugin.
	 *
	 * Since everything within the plugin is registered via hooks,
	 * then kicking off the plugin from this point in the file does
	 * not affect the page life cycle.
	 *
	 * @since    1.0.0
	 */
	function run_ljc_setting() {
		//echo get_option( 'template' );
		$plugin = new Ljc_Setting();
		$plugin_name = $plugin->get_plugin_name();
		$plugin_version = $plugin->get_version();
		$plugin->get_loader()->add_action('woocommerce_product_after_variable_attributes', LJC_WooVariations::get_instance(), 'variation_settings_fields',10,3);
		$plugin->get_loader()->add_action('woocommerce_save_product_variation', LJC_WooVariations::get_instance(), 'save_variation_settings_fields',10,2);
		$plugin->get_loader()->add_action('wp_get_attachment_image_attributes', LJC_HookPublic::get_instance(), 'ljc_woocommerce_thumbnail',10,3);
		$plugin->get_loader()->add_action('woocommerce_after_shop_loop_item', LJC_HookPublic::get_instance(), 'ljc_action_woocommerce_after_shop_loop_item',10,2);
		$plugin->get_loader()->add_filter('woocommerce_product_single_add_to_cart_text', LJC_HookPublic::get_instance(), 'ljc_woo_custom_cart_button_text', 10, 1);
		$plugin->get_loader()->add_filter('woocommerce_is_sold_individually', LJC_WooTemplate::get_instance(), 'wc_remove_all_quantity_fields', 10, 2);
		$plugin->get_loader()->add_filter('wc_add_to_cart_message', LJC_WooTemplate::get_instance(), 'wc_add_to_cart_message', 10);
		//$plugin->get_loader()->add_filter('woocommerce_product_add_to_cart_text', LJC_HookPublic::get_instance(), 'ljc_woo_custom_cart_button_text');
		//breadcrumb
        //this is before the main content
        $plugin->get_loader()->add_filter('woocommerce_before_main_content', LJC_WooTemplate::get_instance(), 'woocommerce_before_main_content', 10);
        //$plugin->get_loader()->add_filter('woocommerce_single_product_summary', LJC_WooTemplate::get_instance(), 'woocommerce_before_main_content', 10);
        //this is inside the main content
        //$plugin->get_loader()->add_filter('avada_before_main', LJC_WooTemplate::get_instance(), 'woocommerce_before_main_content', 10);
        $plugin->get_loader()->add_filter('woocommerce_archive_description', LJC_WooTemplate::get_instance(), 'avada_before_body_content', 10);
		//breadcrumb
		//hook before adding to add cart
		$plugin->get_loader()->add_filter('woocommerce_add_cart_item_data', LJC_WooCart::get_instance(), 'woo_custom_add_to_cart', 10, 3);
		$plugin->get_loader()->add_filter('woocommerce_add_to_cart', LJC_WooCart::get_instance(), 'woocommerce_add_to_cart');
		//check cart items
		//$plugin->get_loader()->add_filter('woocommerce_check_cart_items', LJC_WooCart::get_instance(), 'woocommerce_check_cart_items');
		//hook for new order
		//must remove any option fields
		$plugin->get_loader()->add_filter('woocommerce_checkout_order_processed', LJC_WooCart::get_instance(), 'woocommerce_checkout_order_processed');
		$plugin->get_loader()->add_filter('woocommerce_cart_item_removed', LJC_WooCart::get_instance(), 'woocommerce_cart_item_removed');
		//short code
		LJC_CartShortcode::get_instance();
		LJC_BreadcrumbShortcode::get_instance();
		//ajax save selection as pair
		$plugin->get_loader()->add_filter('wp_ajax_save_selection', LJC_SaveCartDB::get_instance(), 'ajax_save_selection', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_save_selection', LJC_SaveCartDB::get_instance(), 'ajax_save_selection', 10, 1);
		//ajax remove selection as pair
		$plugin->get_loader()->add_filter('wp_ajax_remove_save_selection', LJC_SaveCartDB::get_instance(), 'ajax_remove_save_selection', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_remove_save_selection', LJC_SaveCartDB::get_instance(), 'ajax_remove_save_selection', 10, 1);
		//ajax get save selection as pair
		$plugin->get_loader()->add_filter('wp_ajax_get_save_selection', LJC_SaveCartDB::get_instance(), 'ajax_get_save_selection', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_get_save_selection', LJC_SaveCartDB::get_instance(), 'ajax_get_save_selection', 10, 1);
		//add to WC Cart
		$plugin->get_loader()->add_filter('wp_ajax_ljc_add_to_cart', LJC_WooCart::get_instance(), 'ajax_ljc_add_to_cart', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_ljc_add_to_cart', LJC_WooCart::get_instance(), 'ajax_ljc_add_to_cart', 10, 1);
		//LJC Cart
		$plugin->get_loader()->add_filter('wp_ajax_ljc_remove_setting', LJC_CartAjax::get_instance(), 'ajax_ljc_remove_setting', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_ljc_remove_setting', LJC_CartAjax::get_instance(), 'ajax_ljc_remove_setting', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_ljc_remove_diamond', LJC_CartAjax::get_instance(), 'ajax_ljc_remove_diamond', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_ljc_remove_diamond', LJC_CartAjax::get_instance(), 'ajax_ljc_remove_diamond', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_ljc_reset_selection', LJC_CartAjax::get_instance(), 'ajax_ljc_reset_selection', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_ljc_reset_selection', LJC_CartAjax::get_instance(), 'ajax_ljc_reset_selection', 10, 1);
		$plugin->get_loader()->add_action('template_redirect', LJC_CartEntity::get_instance(), 'ljc_template_redirect');
		$plugin->get_loader()->add_filter('ljc_avada_title_bar', LJC_Avada::get_instance(), 'the_title', 10, 2);
		$plugin->get_loader()->add_filter('fusion_breadcrumbs_defaults', LJC_Avada::get_instance(), 'fusion_breadcrumbs_defaults',10,1);
		$plugin->get_loader()->add_action('template_redirect', LJC_DiamondRecentViewDB::get_instance(), 'init', 10);
		$plugin->get_loader()->add_filter('wp_ajax_ljc_comparison_store_db', LJC_DiamondComparisonDB::get_instance(), 'ajax_ljc_comparison_store_db', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_ljc_comparison_store_db', LJC_DiamondComparisonDB::get_instance(), 'ajax_ljc_comparison_store_db', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_ljc_del_comparison_db', LJC_DiamondComparisonDB::get_instance(), 'ajax_ljc_del_comparison_db', 10, 1);
		$plugin->get_loader()->add_filter('wp_ajax_nopriv_ljc_del_comparison_db', LJC_DiamondComparisonDB::get_instance(), 'ajax_ljc_del_comparison_db', 10, 1);
		$plugin->get_loader()->add_filter('woocommerce_product_tabs', LJC_WooTemplate::get_instance(), 'woo_reorder_tabs', 98, 1);
		$plugin->get_loader()->add_filter('woocommerce_product_tabs', LJC_WooTemplate::get_instance(), 'woo_rename_tabs', 98, 1);
		$plugin->get_loader()->add_filter('woocommerce_product_description_heading', LJC_WooTemplate::get_instance(), 'ljc_product_description_heading', 98, 1);
		$plugin->get_loader()->add_filter('woocommerce_product_additional_information_heading', LJC_WooTemplate::get_instance(), 'ljc_product_additional_information_heading', 98, 1);
		//session start
		LJC_Session::get_instance();
		
		$plugin->run();
	}
	add_action('plugins_loaded', 'run_ljc_setting');
}//check if woo is active

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
function ljc_get_data(){
	return LJC_CartDB::get_instance()->get();
}
/**
 * Use to get cart items
 * */
function ljc_get_cart_items(){
	return LJC_CartQuery::get_instance()->get_cart_items();
}
/**
 * Use to get cart particularly settings items
 * */
function ljc_get_cart_settings(){
	return LJC_CartQuery::get_instance()->get_cart_settings();
}
/**
 * Use to get cart particularly diamonds items
 * */
function ljc_get_cart_diamonds(){
	return LJC_CartQuery::get_instance()->get_cart_diamonds();
}
/**
 * customize wc_get_product
 * return object else return false if empty
 * */
function ljc_get_cart_settings_data(){
	return LJC_CartQuery::get_instance()->cart_settings_info();
}
/**
 * customize wc_get_product
 * return object else return false if empty
 * */
function ljc_get_cart_diamonds_data(){
	return LJC_CartQuery::get_instance()->cart_diamonds_info();
}
/**
 * use wc_get_product return
 * more info https://docs.woocommerce.LJC_Sessioncom/wc-apidocs/class-WC_Product.html
 * */
function ljc_wc_cart_diamond_info(){
	return LJC_CartQuery::get_instance()->wc_cart_diamonds_info();
}
/**
 * use wc_get_product return
 * more info https://docs.woocommerce.com/wc-apidocs/class-WC_Product.html
 * */
function ljc_wc_cart_settings_info(){
	return LJC_CartQuery::get_instance()->wc_cart_settings_info();
}
//check if ljc cart is finalize
function ljc_check_cart_is_final(){
	
}
//save selections
function ljc_get_saved_selection(){
	return LJC_SaveCartDB::get_instance()->get_db();
}
function ljc_loop_saved_selection(){
	//save_selection_pair
	$db_select = ljc_get_saved_selection();
	$select = $db_select['save_selection_pair'];
	$add_select = array();
	if( $select && count($select) > 0 ){
		foreach($select as $k => $v_array){
			if( is_array($v_array) ){
				$add_select[] = $v_array;
			}
		}
		return $add_select;
	}
	return false;
}
function ljc_query_saved_selection(){
	return LJC_CartQuery::get_instance()->saved_selections_info();
}
function ljc_get_cat_setting(){
	$config = ljc_config('woo_categories');
	return $config['setting'];
}
function ljc_get_cat_diamond(){
	$config = ljc_config('woo_categories');
	return $config['diamond'];
}
/**
 * get saved selections
 * return wc_get_product
 * */
function ljc_get_save_selections(){
	$get_saved_selections = array();
	$saved_selections = ljc_query_saved_selection();
	$idx_count = 0;
	$idx_cnt = 0;
	if( $saved_selections ){
		foreach($saved_selections as $k => $v){//loop saved_selections
			if( is_array($v) ){//if is_array
				$setting = '';
				$diamond = '';
				$selection = '';
				foreach($v as $key => $val){
					if( $key == 'settings'){
						$setting = $val->get_title();
						$idx_count++;
					}
					if( $key == 'diamond'){
						$diamond = $val->get_title();
						$idx_count++;
					}
				}
				$selection = $setting.' - '.$diamond;
				$get_saved_selections[] = $selection;
				$idx_cnt++;
				$idx_count = 0;
			}//if is_arrayLJC_Session
		}//loop saved_selections
		if( count($get_saved_selections) > 0 ){
			return $get_saved_selections;
		}else{
			return false;
		}
	}
	return false;
}
function ljc_create_custom_ring($setting_id, $diamond_id, $setting_ring_size){
	require_once(ABSPATH . 'rapnet/rapnet_dls.php');
	if( function_exists('create_custom_ring') ){
		
		$setting = wc_get_product($setting_id);
		$strSettingSku = '';
		if( $setting->get_sku() ){
			$strSettingSku = $setting->get_sku();
		}
		
		$diamond = wc_get_product($diamond_id);
		$strDiamondSku = '';
		if( $diamond->get_sku() ){
			$strDiamondSku = $diamond->get_sku();
		}
		
		$fltRingPrice = $setting->get_price() + $diamond->get_price();
		$fltRingSize = $setting_ring_size;
		return create_custom_ring($strSettingSku, $strDiamondSku, $fltRingPrice, $fltRingSize);
	}
	return false;
}
function ljc_has_setting(){
	return LJC_CartDB::get_instance()->has_setting();
}
function ljc_has_diamond(){
	return LJC_CartDB::get_instance()->has_diamond();
}
function ljc_show_breadcrumb($show = false){
	return $show;
}
function ljc_get_session_id(){
	return LJC_Session::get_instance()->get_id();
}

<?php

/**
 * Fired during plugin activation
 *
 * @link       apysais.com
 * @since      1.0.0
 *
 * @package    Ljc_Setting
 * @subpackage Ljc_Setting/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ljc_Setting
 * @subpackage Ljc_Setting/includes
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Ljc_Setting_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       apysais.com
 * @since      1.0.0
 *
 * @package    Ljc_Setting
 * @subpackage Ljc_Setting/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ljc_Setting
 * @subpackage Ljc_Setting/includes
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Ljc_Setting_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ljc-setting',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}

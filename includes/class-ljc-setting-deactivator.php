<?php

/**
 * Fired during plugin deactivation
 *
 * @link       apysais.com
 * @since      1.0.0
 *
 * @package    Ljc_Setting
 * @subpackage Ljc_Setting/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ljc_Setting
 * @subpackage Ljc_Setting/includes
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Ljc_Setting_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
